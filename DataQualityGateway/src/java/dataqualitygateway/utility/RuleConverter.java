/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.utility;

import dataqualitygateway.dto.enums.Rule;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author SandroSchedl
 */
public class RuleConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        if(string == null)
            return null;
        
        return Rule.valueOf(string.toUpperCase());
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if ( o == null) return "";
        return ((Rule)o).getName();
    }
    
}
