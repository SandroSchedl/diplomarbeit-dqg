/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author max
 */
public class RuleHelper implements Serializable{

    String[] countries = Locale.getISOCountries();

    String[] symbols = {"^", "°", "!", "§", "$", "%", "&", "/", "{", "(,", "[", ")", "]", "=", "}", "?", "´", "`", "+", "*", "~", "#", "'", "-", "_", "<", ">", "|", "²", "³"};
    String[] latin = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    String[] europeanCountries = {"Kosovo","Schweden", "Sweden", "Hungary","Ungarn", "Poland","Polen", "Switzerland","Schweiz", "Slovenia","Slowenien", "Austria","Österreich", "Estonia","Estland", "Lithuania","Litauen", "Vatican","Vatikanstaat", "Bulgaria","Bulgarien", "Latvia","Lettland", "Netherlands","Niederlande", "Serbia","Serbien", "Malta","Dänemark", "Montenegro", "Denmark", "Finland", "Kazakhstan","Kasachstan", "Germany","Deutschland", "Luxembourg","Luxemburg", "Romania","Rumänien", "UK", "United Kingdom", "Vereinigtes Königreich", "England", "Schottland", "Scotland", "Wales", "Nordirland", "Northern Ireland", "Georgia", "Georgien", "Turkey","Türkei", "Ireland", "Irland", "Belgium", "Belgien","Moldova", "Moldawien","Spain","Spanien", "Armenia","Armenien", "France", "Frankreich", "Czech Republic", "Tschechien","Azerbaijan","Aserbaidschan", "Andorra", "Croatia","Kroatien", "Iceland", "Island", "Monaco", "Greece","Griechenland", "Zypern","Cyprus", "Bosnia","Bosnien", "Albania","Albanien", "Italy", "Italien", "Slowakei","Slovakia", "Russia", "Russland", "Ukraine","Ukraine", "Portugal", "Belarus", "Liechtenstein", "Weißrussland", "Macedonia","Norwegen", "Norway", "San Marino"};

    public RuleHelper() {
//        Properties prop = new Properties();
//        InputStream input = null;
////        for(int idx = 0;idx<symbol.length(); idx++){
////            symbols.add(symbol.)
////        }
//        try {
//
//            input = new FileInputStream("countries.properties");
//
//            prop.load(input);
//
//            Enumeration<?> e = prop.propertyNames();
//            while (e.hasMoreElements()) {
//                String key = (String) e.nextElement();
//                String value = prop.getProperty(key);
//                europeanCountries.add(value);
//            }
//
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        } finally {
//            if (input != null) {
//                try {
//                    input.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
    }

    public String[] getCountries() {
        return countries;
    }

    public void setCountries(String[] countries) {
        this.countries = countries;
    }

    public String[] getSymbols() {
        return new String[] {"^", "°", "!", "§", "$", "%", "&", "/", "{", "(,", "[", ")", "]", "=", "}", "?", "´", "`", "+", "*", "~", "#", "'", "-", "_", "<", ">", "|", "²", "³"};
    }

    public void setSymbols(String[] symbols) {
        this.symbols = symbols;
    }

    public String[] getLatin() {
        return latin;
    }

    public void setLatin(String[] latin) {
        this.latin = latin;
    }

    public String[] getEuropeanCountries() {
        return europeanCountries;
    }

    public void setEuropeanCountries(String[] europeanCountries) {
        this.europeanCountries = europeanCountries;
    }

//    public static void main(String[] args) {
//       
//
//    }
    public List<String> seperator(String s){
        List<String> list = new ArrayList<>();
        list = Arrays.asList(s.split(","));
        return list;
    }

}
