/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import org.bson.BsonArray;
import org.bson.BsonElement;
import org.bson.BsonString;

/**
 *
 * @author SandroSchedl
 */
public class Template implements Serializable{
    private String name;
    private List<Column> columnList;
    private List<Result> previousResults;
    private int lastId;

    public Template(){
        this("");
    }
    
    public Template(String name) {
        this(name,new LinkedList<Column>());
    }

    public Template(String name, List<Column> columnList) {
        this.name = name;
        this.columnList = columnList;
        this.previousResults = new LinkedList<>();
    }
    
    public Template(Template t){
        this(t.getName());
        this.columnList = new LinkedList<>(t.getColumnList());
        this.previousResults = new LinkedList<>(t.getPreviousResults());
        this.lastId = t.getLastId();
    }
    
    public void addResult(Result r){
        previousResults.add(r);
    }
    
    public List<BsonElement> getColumnsAsBsonObject() {
        List<BsonElement> retVal = new LinkedList<>();
        
        for(Column c : columnList){
            retVal.add(c.getColumnAsBsonObject());
        }
        
        return retVal;
    }
    
    public BsonArray getResultsAsBsonArray() {
        BsonArray retVal = new BsonArray();
        
        for(Result r : previousResults){
            retVal.add(r.getResultIdAsBsonValue());
        }
        
        return retVal;
    }
    
    public int getLastId() {
        return lastId;
    }
    
    public int increaseAndGetLastId() {
        return ++lastId;
    }
    
    public void setLastId(int lastId) {
        this.lastId = lastId;
    }

    public List<Result> getPreviousResults() {
        return previousResults;
    }

    public void setPreviousResults(List<Result> previousResults) {
        this.previousResults = previousResults;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Column> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<Column> columnList) {
        this.columnList = columnList;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Template other = (Template) obj;
        
        return Objects.equals(this.name, other.name);
    }    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.name);
        return hash;
    }

    
    
    @Override
    public String toString() {
        return "Template{" + "name=" + name + ", columnList=" + columnList + ", previousResults=" + previousResults + ", lastId=" + lastId + '}';
    }
    
    
    
}
