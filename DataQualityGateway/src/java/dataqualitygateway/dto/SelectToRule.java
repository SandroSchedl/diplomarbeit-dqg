/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto;

import dataqualitygateway.dto.enums.Rule;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import org.bson.BsonArray;
import org.bson.BsonInt32;

/**
 *
 * @author SandroSchedl
 */
public class SelectToRule implements Serializable{
        
    protected List<Select> chooserList;
    
    public List<Rule> getRules() {
        List<Rule> ruleList = new LinkedList<>();
        for (Select s : chooserList) {
            if (s.isChooser()) {
                ruleList.add(s.getR());
            }
        }

        return ruleList;
    }

    public void setRules(List<Integer> ruleIdList) {

        chooserList.forEach((s) -> {
            s.setChooser(false);
            for (int id : ruleIdList) {
                if (id == s.getR().getId()) {
                    s.setChooser(true);
                }
            }
        });
    }

    protected BsonArray getRulesAsBsonArray() {
        BsonArray retVal = new BsonArray();

        for (Rule r : getRules()) {
            retVal.add(new BsonInt32(r.getId()));
        }

        return retVal;
    }
}
