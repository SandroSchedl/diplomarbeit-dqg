/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import org.bson.BSONObject;
import org.bson.BasicBSONObject;
import org.bson.Document;

/**
 *
 * @author max
 */
public class Group extends SelectToRule implements Serializable {
    private int groupId; 
    private String groupName;

    public Group(int groupId, String groupName) {
        this.groupId = groupId;
        this.groupName = groupName;
    }

    public Group(int groupId, String groupName, List<Select> groupList) {
        this.groupId = groupId;
        this.groupName = groupName;
        this.chooserList = groupList;
    }

    public Document getGroupAsBsonDocument(){
        return new Document("_id", groupId).append("name", groupName).append("ruleList", getRulesAsBsonArray());
    }
    
    public Group(int groupId) {
        this.groupId = groupId;
    }

    public int getGroupId() {
        return groupId;
    }

    public List<Select> getGroupList() {
        return chooserList;
    }

    public void setGroupList(List<Select> groupList) {
        this.chooserList = groupList;
    }




    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return "Group{" + "groupId=" + groupId + ", groupName=" + groupName + ", chooserList=" + chooserList + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Group other = (Group) obj;
        if (this.groupId != other.groupId) {
            return false;
        }
        if (!Objects.equals(this.groupName, other.groupName)) {
            return false;
        }
        if (!Objects.equals(this.chooserList, other.chooserList)) {
            return false;
        }
        return true;
    }
    

   
    
    
}
