/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto;

import com.mongodb.BasicDBObject;
import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import org.bson.BSONObject;
import org.bson.BsonDocument;
import org.bson.BsonInt32;
import org.bson.BsonObjectId;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.Document;

/**
 *
 * @author max
 */
public class CustomizedRule implements Serializable {

    private int valueRangeLeft, valueRangeRight;
    private String country;
    private String bannedWords;
    private String dateFormat;
    private String parameter;
   
    public CustomizedRule(int valueRangeLeft, int valueRangeRight, String country, String bannedWords, String dateFormat, String parameter) {
        this.valueRangeLeft = valueRangeLeft;
        this.valueRangeRight = valueRangeRight;
        this.country = country;
        this.bannedWords = bannedWords;
        this.dateFormat = dateFormat;
        this.parameter = parameter;
    }

    public CustomizedRule(int valueRangeLeft, int valueRangeRight) {
        this.valueRangeLeft = valueRangeLeft;
        this.valueRangeRight = valueRangeRight;
    }

    public CustomizedRule(String country) {
        this.country = country;
    }
   
    
    
    
    public BsonDocument getAsBsonDocument() {
        return new BsonDocument("min", new BsonInt32(valueRangeLeft)).append("max", new BsonInt32(valueRangeRight))
                .append("country", new BsonString(country)).append("bannedWords", new BsonString(bannedWords))
                .append("dateFormat", new BsonString(dateFormat)).append("parameter", new BsonString(parameter));
    }

    public int getValueRangeLeft() {
        return valueRangeLeft;
    }

    public void setValueRangeLeft(int valueRangeLeft) {
        this.valueRangeLeft = valueRangeLeft;
    }

    public int getValueRangeRight() {
        return valueRangeRight;
    }

    public void setValueRangeRight(int valueRangeRight) {
        this.valueRangeRight = valueRangeRight;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBannedWords() {
        return bannedWords;
    }

    public void setBannedWords(String bannedWords) {
        this.bannedWords = bannedWords;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.valueRangeLeft;
        hash = 89 * hash + this.valueRangeRight;
        hash = 89 * hash + Objects.hashCode(this.country);
        hash = 89 * hash + Objects.hashCode(this.bannedWords);
        hash = 89 * hash + Objects.hashCode(this.dateFormat);
        hash = 89 * hash + Objects.hashCode(this.parameter);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CustomizedRule other = (CustomizedRule) obj;
        if (this.valueRangeLeft != other.valueRangeLeft) {
            return false;
        }
        if (this.valueRangeRight != other.valueRangeRight) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        if (!Objects.equals(this.bannedWords, other.bannedWords)) {
            return false;
        }
        if (!Objects.equals(this.dateFormat, other.dateFormat)) {
            return false;
        }
        if (!Objects.equals(this.parameter, other.parameter)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        if(this.valueRangeRight!=0){
            return "ValueRange: "+valueRangeLeft+" - "+valueRangeRight;
        }
        else{
            return "Customized: " +country;
        }
//        return "CustomizedRule{" + "valueRangeLeft=" + valueRangeLeft + ", valueRangeRight=" + valueRangeRight + ", country=" + country + ", bannedWords=" + bannedWords + ", dateFormat=" + dateFormat + ", parameter=" + parameter + '}';
    }


}


