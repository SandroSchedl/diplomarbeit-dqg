/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto;

import dataqualitygateway.dto.enums.DataType;
import dataqualitygateway.dto.enums.Rule;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import java.util.Objects;
import org.bson.BsonBoolean;
import org.bson.BsonElement;

/**
 *
 * @author SandroSchedl
 */
public class Data implements HasPDFValue, Serializable{

    private Object value;
    private DataType typeOfValue;

//speichert die Regeln, auf welche diesr Wert überprüft werden soll und ob die Überprüfung erfolgreich war
    private Map<Rule, Boolean> ruleMap = new HashMap<>();

    private int errorCounter = 0;

    public Data(Object value) {
        this.value = value;
        if (value != null) {
            this.typeOfValue = DataType.getTypeOfValue(value.getClass());
        } else {
            this.typeOfValue = DataType.NULL;
        }

    }

    public List<BsonElement> getRulesAsBson() {
        List<BsonElement> ele = new LinkedList<>();

        for (Entry<Rule, Boolean> entry : ruleMap.entrySet()) {
            ele.add(new BsonElement(String.valueOf(entry.getKey().getId()), new BsonBoolean(entry.getValue())));
        }

        return ele;
    }
    
    @Override
    public String getPDFValue(){
        return String.valueOf(value);
    }

    @Override
    public boolean isValueCorrect() {
        return errorCounter == 0;
    }

    public void increaseErrorCounter(){
        errorCounter++;
    }
    
    public void addRule(Rule rule, Boolean state) {
            ruleMap.put(rule, state);
    }

    public Map<Rule, Boolean> getRuleMap() {

        return ruleMap;
    }
    
    public void setRuleMap(Map<Rule, Boolean> ruleMap) {
        this.ruleMap = ruleMap;
    }

    public Data() {
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public void setTypeOfValue(DataType typeOfValue) {
        this.typeOfValue = typeOfValue;
    }

    public DataType getTypeOfValue() {
        return typeOfValue;
    }

    @Override
    public String toString() {
        if(typeOfValue == DataType.NULL)    return "";
        return value.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Data other = (Data) obj;
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        if (this.typeOfValue != other.typeOfValue) {
            return false;
        }
        return true;
    }

    public int getErrorCounter() {
        return errorCounter;
    }

    public void setErrorCounter(int errorCounter) {
        this.errorCounter = errorCounter;
    }

}
