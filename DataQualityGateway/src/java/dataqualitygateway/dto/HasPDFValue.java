/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto;

/**
 *
 * @author SandroSchedl
 */
public interface HasPDFValue {
    String getPDFValue();
    boolean isValueCorrect();
}
