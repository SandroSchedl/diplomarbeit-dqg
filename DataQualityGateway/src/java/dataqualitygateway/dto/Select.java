/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto;

import dataqualitygateway.dto.enums.Rule;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author max
 */
//This class represents one row of the choosing screen 
public class Select implements Serializable, Selectable{
    private Rule r; 
    private boolean chooser;
    private CustomizedRule cr;
    

    public Select(Rule r, boolean chooser) {
        this.r = r;
        this.chooser = chooser;
    }

    public Select(Rule r, boolean chooser, CustomizedRule cr) {
        this.r = r;
        this.chooser = chooser;
        this.cr = cr;
    }
    
    

    public Select() {
    }

    public Rule getR() {
        return r;
    }

    public void setR(Rule r) {
        this.r = r;
    }

    public boolean isChooser() {
        return chooser;
    }

    public void setChooser(boolean chooser) {
        this.chooser = chooser;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Objects.hashCode(this.r);
        hash = 73 * hash + (this.chooser ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Select other = (Select) obj;
        if (this.chooser != other.chooser) {
            return false;
        }
        if (this.r != other.r) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Select{" + "r=" + r + ", chooser=" + chooser + '}';
    }

    public CustomizedRule getCr() {
        return cr;
    }

    public void setCr(CustomizedRule cr) {
        this.cr = cr;
    }
    
    
}
