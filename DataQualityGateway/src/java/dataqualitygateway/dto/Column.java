/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto;

import dataqualitygateway.dto.enums.Rule;
import dataqualitygateway.dto.enums.DataType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonElement;
import org.bson.BsonInt32;
import org.bson.BsonNull;
import org.bson.BsonString;
import org.bson.BsonType;
import org.bson.BsonValue;

/**
 *
 * @author SandroSchedl
 */
public class Column extends SelectToRule implements HasPDFValue, Serializable {

    private String name;
    private DataType dataType;
    private CustomizedRule customRule;
    private List<SelectGroup> groupList;
    private List<GenericPojo> compareList = new LinkedList<>();
    

    public Column(String name) {
        this.name = name;
        this.dataType = DataType.STRING;
        this.chooserList = newChooserList(Rule.getRules());
        this.groupList = new LinkedList<>();
//        ruleList.add(Rule.ATSIGN);
//        ruleList.add(Rule.CAPSCHECKER);
//        ruleList.add(Rule.CREDITCARDCHECKER);
//        ruleList.add(Rule.DATECHECKER);
//        ruleList.add(Rule.ISCOUNTRY);
    }

    public BsonElement getColumnAsBsonObject() {
        return new BsonElement(name, new BsonDocument("dataType", new BsonString(dataType.getName()))
                .append("ruleList", getRulesAsBsonArray())
                .append("customRule", getCustomRuleAsBsonObject()));

    }

    public void addSelectGroup(SelectGroup sg) {
        this.groupList.add(sg);
    }

    private BsonValue getCustomRuleAsBsonObject() {
        if (customRule != null) {
            return customRule.getAsBsonDocument();
        }

        return new BsonNull();
    }

    @Override
    public String getPDFValue() {
        return name;
    }

    @Override
    public boolean isValueCorrect() {
        return true;
    }

    public CustomizedRule getCustomRule() {
        return customRule;
    }

    public void setCustomRule(CustomizedRule customRule) {
        this.customRule = customRule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType type) {
        this.dataType = type;
    }

    @Override
    public String toString() {
        return "Column{" + "name=" + name + ", type=" + dataType + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.name);
        return hash;
    }

    public List<Select> getChooserList() {
        return chooserList;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Column other = (Column) obj;
        return Objects.equals(this.name, other.name);
    }

    public void setChooserList(List<Select> chooserList) {
        this.chooserList = chooserList;
    }

    public List<Select> newChooserList(List<Rule> ruleList) {
        List<Select> selList = new ArrayList<>();
        for (Rule r : ruleList) {
            selList.add(new Select(r, false));
        }
        return selList;
    }

    public List<SelectGroup> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<SelectGroup> grList) {

        this.groupList = grList;
//         else {
//            for (SelectGroup gr : groupList) {
//                for (SelectGroup newGr : grList) {
//                    this.groupList.add(newGr);
//                    if (gr.getG().getGroupId() == newGr.getG().getGroupId()) {
//                        this.groupList.remove(newGr);
//                    }
//                }
//            }
//        }

    }

    public List<Select> getShowChooserList() {
        List<Select> retValList = new LinkedList<>();

        for (Select s : this.chooserList) {
            if (s.isChooser()) {
                retValList.add(s);
            }
        }
        return retValList;
    }

    public List<SelectGroup> getShowGroupList() {
        List<SelectGroup> retValList = new LinkedList<>();

        for (SelectGroup gs : this.groupList) {
            if (gs.isChooser()) {
                retValList.add(gs);
            }
        }
        return retValList;
    }

    public List<GenericPojo> getCompareList() {
        return compareList;
    }

    public void setCompareList(List<GenericPojo> compareList) {
        this.compareList = compareList;
    }
     

}
