/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto;

import dataqualitygateway.dto.enums.Rule;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author max
 */
public class DataCheck implements Serializable {
    //result of the examination
    private boolean check;
    
    //object which has been checked 
    private Object o;
    
    //List of Object that have been checked
    List<Object> oList;
    
    //The used rule
    private Rule r; 

    public DataCheck() {
    }

    public DataCheck(boolean check, Object o, Rule r) {
        this.check = check;
        this.o = o;
        this.r = r;
    }

    
    
    public DataCheck(boolean check, Object o) {
        this.check = check;
        this.o = o;
    }

    public DataCheck(boolean check, List<Object> oList) {
        this.check = check;
        this.oList = oList;
    }

    public DataCheck(boolean b) {
        this.check = b;
    }

    public DataCheck(boolean check, Rule r) {
        this.check = check;
        this.r = r;
    }

    @Override
    public String toString() {
        return "DataCheck{" + "check=" + check + ", o=" + o + ", oList=" + oList + ", r=" + r + '}';
    }

   

 

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public Object getO() {
        return o;
    }

    public void setO(Object o) {
        this.o = o;
    }

    public List<Object> getoList() {
        return oList;
    }

    public void setoList(List<Object> oList) {
        this.oList = oList;
    }

    public Rule getR() {
        return r;
    }

    public void setR(Rule r) {
        this.r = r;
    }

}
