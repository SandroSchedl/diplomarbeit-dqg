/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto;

import java.util.Objects;

/**
 *
 * @author max
 */

public class SelectGroup implements Selectable{
    private boolean chooser;
    private Group g;

    public boolean isChooser() {
        return chooser;
    }

    public void setChooser(boolean chooser) {
        this.chooser = chooser;
    }

    public Group getG() {
        return g;
    }

    public void setG(Group g) {
        this.g = g;
    }

    public SelectGroup() {
    }

    public SelectGroup(boolean chooser, Group g) {
        this.chooser = chooser;
        this.g = g;
    }

    @Override
    public String toString() {
        return "SelectGroup{" + "chooser=" + chooser + ", g=" + g + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SelectGroup other = (SelectGroup) obj;
        if (!Objects.equals(this.g, other.g)) {
            return false;
        }
        return true;
    }
    
    
}
