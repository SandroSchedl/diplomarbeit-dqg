/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto;

import dataqualitygateway.dto.enums.Rule;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.bson.BSONObject;
import org.bson.BasicBSONObject;
import org.bson.BsonDocument;

/**
 *
 * @author SandroSchedl
 */
public class GenericPojo implements Serializable{

    private String recordId;
    
    //Speichert zu jeder Spalte jeweils 1 Wert. 1 GenericPojo entspricht demnach eine Zeile in der Datenbank
    private Map<Column, Data> valueMap = new LinkedHashMap<>();
    private int errorCounter = 0;

    public GenericPojo() {
    }

    public void addValue(Column column, Data data) {

        addRulesToData(column, data);

        valueMap.put(column, data);
    }

    //adds the rules of the coulumn to the data object
    private void addRulesToData(Column c, Data data) {
        for (Rule r : c.getRules()) {
            data.addRule(r, false);
        }

    }

    //returns all values of the record separated by a ;
    public String getValues() {
        StringBuilder s = new StringBuilder();

        for (Data d : valueMap.values()) {
            for(Boolean result : d.getRuleMap().values()){
                if(result.equals(false)){
//                    s.append("(Farbe ändern) ");
                }
            }
            s.append(d.getValue());
            s.append(" // ");
        }
        
        return s.toString();
    }
        
    /*
        Iterates through a GenericPojo and checks if the record is correct.
        If one failed rule is found, it returns false
        If all rules have been successful, it returns true
     */
    public boolean isRecordCorrect(){
        for(Data d : valueMap.values()){
            for(Boolean result : d.getRuleMap().values()){
                if(result.equals(false))
                    return false;
            }
        }
        return true;
    }
    
    //returns all values of the record in a BSON-format
    public BSONObject getRecordAsBsonObject() {
        BSONObject o = new BasicBSONObject();

        for (Map.Entry<Column,Data> entry : valueMap.entrySet()) {
            o.put(entry.getKey().getName(), new BsonDocument("value", entry.getValue().getTypeOfValue().getBsonValue(entry.getValue()))
                    .append("rules", new BsonDocument(entry.getValue().getRulesAsBson())));
        }

        return o;
    }
    
    public BSONObject getRecordAsBsonObjectWithoutValues() {
        BSONObject o = new BasicBSONObject();

        for (Map.Entry<Column,Data> entry : valueMap.entrySet()) {
            o.put(entry.getKey().getName(), new BsonDocument(entry.getValue().getRulesAsBson()));
        }

        return o;
    }
    
    public String getDetailMessage(Column c){
        return valueMap.get(c).toString();
    }
    

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }
    
    public Map<Column, Data> getValueMap() {
        return valueMap;
    }

    public void setValueMap(Map<Column, Data> valueMap) {
        this.valueMap = valueMap;
    }

    @Override
    public String toString() {
        return "GenericPojo{" + "valueMap=" + valueMap + '}';
    }

    public int getErrorCounter() {
       
        return errorCounter;
    }

    public void setErrorCounter(int errorCounter) {
        this.errorCounter = errorCounter;
    }


}
