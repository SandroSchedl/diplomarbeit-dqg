/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto.enums;

import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.CustomizedRule;
import dataqualitygateway.dto.RuleHelper;
import dataqualitygateway.dto.Data;
import dataqualitygateway.dto.GenericPojo;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 *
 * @author max
 */
public enum Rule {
    VALUERANGE(1, "ValueRange", "Rule to check if the given numbers are in the defined Valuerange, by default the checked range is the Integer Size", true) {
        @Override
        public boolean check(Data d) {
            boolean retVal = false;

            try {
                if ((Integer) d.getValue() <= 32000 && (Integer) d.getValue() >= -32000) {
                    retVal = true;
                }
            } catch (Exception e) {
//                try {
//                    if ((Double) d.getValue() <= 32000 && (Double) d.getValue() >= -32000) {
//                        retVal = true;
//                    }
//                } catch (Exception ex) {
//                }
            }
            return retVal;
        }

        @Override
        public boolean check(Data d, List<GenericPojo> list) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean check(Data d, CustomizedRule cr) {
            boolean retVal = false;
            if (d == null) {
                return false;
            }
            if (d.getValue() == null) {
                return false;
            }
            if (cr == null) {
                VALUERANGE.check(d);
            }
            try {
                if ((Integer) d.getValue() <= cr.getValueRangeRight() && (Integer) d.getValue() >= cr.getValueRangeLeft()) {
                    retVal = true;
                }
            } catch (Exception e) {
//                try {
//                    if ((Double) d.getValue() <= cr.getValueRangeRight() && (Double) d.getValue() >= cr.getValueRangeLeft()) {
//                        retVal = true;
//                    }
//                } catch (Exception ex) {
//                    return false;
//                }
              
            }
            return retVal;
        }
    },
    TABLECHECKER(18, "TableChecker", "Rule to check if the value of the column equals a value from the table", true) {
        @Override
        public boolean check(Data d) {
            boolean retVal = false;

            return retVal;
        }

        public boolean check(Data d, List<GenericPojo> list) {
            boolean retVal = false;

            if (d == null) {
                return false;
            }
            if (d.getValue() == null) {
                return false;
            }
            for (GenericPojo gp : list) {
                for (Map.Entry<Column, Data> entry : gp.getValueMap().entrySet()) {
                    if (entry.getValue().equals(d)) {
                        retVal = true;
                    }
                }
            }
            return retVal;
        }

        @Override
        public boolean check(Data d, CustomizedRule cr) {
            boolean retVal = false;
            return retVal;
        }

    },
    //    TYPECHECKER(2, "TypeChecker", "Rule to check if the choosen types are in the column, by default the checked Type is STRING", false) {
    //
    //        @Override
    //        public boolean check(Data d) {
    //            boolean retVal = false;
    //            if (d == null) {
    //                return retVal;
    //            }
    //            DataType dt = d.getTypeOfValue();
    //
    //            if (dt.equals(DataType.STRING)) {
    //                TYPECHECKER.type = "String";
    //                retVal = true;
    //            }
    //            if (dt.equals(DataType.INTEGER)) {
    //                try {
    //                    Integer.parseInt((String) d.getValue());
    //                } catch (Exception e) {
    //                    return false;
    //                }
    //                TYPECHECKER.type = "Integer";
    //                retVal = true;
    //            }
    //            if (dt.equals(DataType.DOUBLE)) {
    //                TYPECHECKER.type = "Double";
    //                try {
    //                    Double.parseDouble((String) d.getValue());
    //                } catch (Exception e) {
    //                    return false;
    //                }
    //                retVal = true;
    //            }
    //            return retVal;
    //        }
    //
    //        @Override
    //        public boolean check(Data d, List<GenericPojo> list) {
    //            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    //        }
    //
    //        @Override
    //        public boolean check(Data d, CustomizedRule cr) {
    //            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    //        }
    //
    //    },
    ISEUROPE(3, "IsEurope", "Rule to check if the wines are from Europe", false) {
        @Override
        public boolean check(Data d) {
            boolean retVal = false;

            RuleHelper c = new RuleHelper();
            for (String s : c.getEuropeanCountries()) {
                if (s.equals(d.getValue())) {
                    retVal = true;
                }
            }

            return retVal;
        }

        @Override
        public boolean check(Data d, List<GenericPojo> list) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean check(Data d, CustomizedRule cr) {
            if (d == null) {
                return false;
            }
            if (d.getValue() == null) {
                return false;
            }
            return ISEUROPE.check(d);
        }

    },
    ISCOUNTRY(4, "IsCountry", "Rule to check if the wines are from a certain country", true) {
        @Override
        public boolean check(Data d) {
            boolean retVal = false;

            if (d.getValue().equals("Austria")) {
                retVal = true;
            }
            return retVal;
        }

        @Override
        public boolean check(Data d, List<GenericPojo> list) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean check(Data d, CustomizedRule cr) {
            boolean retVal = false;
            if (d == null) {
                return false;
            }
            if (d.getValue() == null) {
                return false;
            }
            if (cr == null) {
                ISCOUNTRY.check(d);
            }
            if (d.getValue().equals(cr.getCountry())) {
                retVal = true;
            }
            return retVal;
        }

    },
    ATSIGN(5, "AtSign", "Rule to check if the twitter account of the tester has an @Sign", false) {
        @Override
        public boolean check(Data d) {
            boolean retVal = false;

            if (d.getValue().toString().contains("@")) {
                return true;
            }
            return retVal;
        }

        @Override
        public boolean check(Data d, List<GenericPojo> list) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean check(Data d, CustomizedRule cr) {
            if (d == null) {
                return false;
            }
            if (d.getValue() == null) {
                return false;
            }
            return ATSIGN.check(d);
        }

    },
    //    NOTNULL(6, "NotNull", "Rule to check if a row has a null value object") {
    //
    //        @Override
    //        public boolean check(Data d) {
    //            boolean retVal = false;
    //            if (d == null) {
    //                return retVal;
    //            }
    //            if (d.getValue() != null) {
    //                retVal = true;
    //            }
    //            return retVal;
    //        }
    //
    //    },
    NOSYMBOLS(7, "NoSymbol", "Rule to check that there are no symbols", false) {

        public boolean check(Data d) {
            boolean retVal = true;

            RuleHelper c = new RuleHelper();

            for (String s : c.getSymbols()) {

                if (d.getValue().toString().contains(s)) {
                    retVal = false;

                }

            }
            return retVal;
        }

        @Override
        public boolean check(Data d, List<GenericPojo> list) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean check(Data d, CustomizedRule cr) {
            if (d == null) {
                return false;
            }
            if (d.getValue() == null) {
                return false;
            }
            return NOSYMBOLS.check(d);
        }

    },
    ISWRITINGSYSTEM(8, "IsWritingSystem", "Rule to check if there are any other writing systems used, default it is checked if the Latin Writing System is used", false) {
        @Override
        public boolean check(Data d) {
            boolean retVal = false;
            int error = 0;

//            RuleHelper c = new RuleHelper();
//
//            for (String s : c.getLatin()) {
//
//                if (d.getValue().toString().contains(s.toUpperCase())) {
//                    retVal = true;
//                    
//                }else
//                    error++;
//            }
//            if(error!=0){
//                retVal = false;
//            }
            retVal = d.getValue().toString().matches("\\w+");
            return retVal;
        }

        @Override
        public boolean check(Data d, List<GenericPojo> list) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean check(Data d, CustomizedRule cr) {
            if (d == null) {
                return false;
            }
            if (d.getValue() == null) {
                return false;
            }
            return ISWRITINGSYSTEM.check(d);
        }
    },
    EXISTINGCOUNTRY(9, "ExistingCountry", "Rule to check if the countries are existing countries", false) {
        @Override
        public boolean check(Data d) {
            boolean retVal = false;

            RuleHelper c = new RuleHelper();

            for (String countryCode : c.getCountries()) {

                Locale obj = new Locale("en", countryCode);

                if (d.getValue().equals(obj.getDisplayCountry(obj)) || d.getValue().equals("US") || d.getValue().equals("UK")) {
                    retVal = true;
                }

            }
            return retVal;
        }

        @Override
        public boolean check(Data d, List<GenericPojo> list) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean check(Data d, CustomizedRule cr) {
            if (d == null) {
                return false;
            }
            if (d.getValue() == null) {
                return false;
            }
            return EXISTINGCOUNTRY.check(d);
        }

    },
    //    PLZCHECKER(10, "PLZChecker", "Rule to check if the given PostCode is existing and matches to the given town/village", false) {
    //        @Override
    //        public boolean check(Data d) {
    //            boolean retVal = false;
    //            if (d == null) {
    //                return retVal;
    //            }
    //
    //            return retVal;
    //        }
    //
    //        @Override
    //        public boolean check(Data d, List<GenericPojo> list) {
    //            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    //        }
    //
    //        @Override
    //        public boolean check(Data d, CustomizedRule cr) {
    //            return PLZCHECKER.check(d);
    //        }
    //
    //    },
    CREDITCARDCHECKER(11, "CreditCardChecker", "Rule to check if the given CreditCardNumber is valid", false) {
        //Method to check if it is an valid credicard --> Usage of the Luhn  Algorithm
        @Override
        public boolean check(Data d) {
            boolean retVal = false;

            String str = null;
            try {
                str = (String) d.getValue();
            } catch (Exception e) {
                return false;
            }

            if (str.contains("1")) {
                int[] ints = new int[str.length()];
                for (int i = 0; i < str.length(); i++) {
                    ints[i] = Integer.parseInt(str.substring(i, i + 1));
                }
                for (int i = ints.length - 2; i >= 0; i = i - 2) {
                    int j = ints[i];
                    j = j * 2;
                    if (j > 9) {
                        j = j % 10 + 1;
                    }
                    ints[i] = j;
                }
                int sum = 0;
                for (int i = 0; i < ints.length; i++) {
                    sum += ints[i];
                }
                if (sum % 10 == 0) {
                    retVal = true;
                } else {
                    retVal = false;
                }

            } else {
                retVal = false;
            }
            return retVal;
        }

        @Override
        public boolean check(Data d, List<GenericPojo> list) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean check(Data d, CustomizedRule cr) {
            if (d == null) {
                return false;
            }
            if (d.getValue() == null) {
                return false;
            }
            return CREDITCARDCHECKER.check(d);
        }

    },
    LANGUAGEFILTER(12, "LanguageFilter", "Rule to check if there are any banned words in the Database", true) {
        @Override
        public boolean check(Data d) {
            boolean retVal = false;

            return retVal;
        }

        @Override
        public boolean check(Data d, List<GenericPojo> list) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean check(Data d, CustomizedRule cr) {
            RuleHelper r = new RuleHelper();
            boolean retVal = true;
            if (d == null) {
                return false;
            }
            if (d.getValue() == null) {
                return false;
            }
            for (String s : r.seperator(cr.getBannedWords())) {
                if (s.equals(d.getValue())) {
                    retVal = false;
                }
            }
            return retVal;
        }

    },
    GENDERFILTER(13, "GenderFilter", "Rule to check if all Persons have a gender in there row", false) {
        @Override
        public boolean check(Data d) {
            boolean retVal = false;

            if (d.getValue().equals("Male") || d.getValue().equals("Female") || d.getValue().equals("Other")) {
                retVal = true;
            }
            return retVal;
        }

        @Override
        public boolean check(Data d, List<GenericPojo> list) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean check(Data d, CustomizedRule cr) {
            if (d == null) {
                return false;
            }
            if (d.getValue() == null) {
                return false;
            }
            return GENDERFILTER.check(d);
        }
    },
    //    DUPLICATEFIELDNAME(14, "DuplicateFieldName", "Rule to check if there are any duplicate field names", false) {
    //        @Override
    //        public boolean check(Data d) {
    //            boolean retVal = false;
    //            if (d == null) {
    //                return retVal;
    //            }
    //
    //            return retVal;
    //        }
    //
    //        @Override
    //        public boolean check(Data d, List<GenericPojo> list) {
    //            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    //        }
    //
    //        @Override
    //        public boolean check(Data d, CustomizedRule cr) {
    //           return DUPLICATEFIELDNAME.check(d);
    //        }
    //    },
    DATECHECKER(15, "DateChecker", "Rule to check if there are only valid dates in the Database", true) {
        @Override
        public boolean check(Data d) {
            boolean retVal = false;

            String date;
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            try {
                date = (String) d.getValue();
                df.parse(date);
                retVal = true;
            } catch (Exception e) {
            }
            df = new SimpleDateFormat("dd-MM-yyyy");
            try {
                date = (String) d.getValue();
                df.parse(date);
                retVal = true;
            } catch (Exception e) {
            }
            df = new SimpleDateFormat("dd-MM-yy");
            try {
                date = (String) d.getValue();
                df.parse(date);
                retVal = true;
            } catch (Exception e) {
            }
            return retVal;
        }

        @Override
        public boolean check(Data d, List<GenericPojo> list) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean check(Data d, CustomizedRule cr) {
            boolean retVal = false;
            if (d == null) {
                return false;
            }
            if (d.getValue() == null) {
                return false;
            }
            String date;
            SimpleDateFormat df = new SimpleDateFormat(cr.getDateFormat());
            try {
                date = (String) d.getValue();
                df.parse(date);
                retVal = true;
            } catch (Exception e) {
            }
            return retVal;
        }

    },
    PARAMETERCHECK(16, "ParameterCheck", "Rule to check if the values match with the given list", true) {
        @Override
        public boolean check(Data d) {
            boolean retVal = false;

            return retVal;
        }

        @Override
        public boolean check(Data d, List<GenericPojo> list) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean check(Data d, CustomizedRule cr) {
            RuleHelper r = new RuleHelper();
            boolean retVal = false;
            if (d == null) {
                return false;
            }
            if (d.getValue() == null) {
                return false;
            }
            for (String s : r.seperator(cr.getParameter())) {
                if (s.equals(d.getValue())) {
                    retVal = true;
                }
            }
            return retVal;
        }

    },
    CAPSCHECKER(17, "CAPSChecker", "Rule to check if all values are written in Caps it will return true", false) {
        @Override
        public boolean check(Data d) {
            boolean retVal = false;

            if (d.getValue().toString().equals(d.getValue().toString().toUpperCase())) {
                retVal = true;
            }
            return retVal;
        }

        @Override
        public boolean check(Data d, List<GenericPojo> list) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean check(Data d, CustomizedRule cr) {
            if (d == null) {
                return false;
            }
            if (d.getValue() == null) {
                return false;
            }
            return CAPSCHECKER.check(d);
        }

    };

    private int id;
    private String name;
    private String description;
    private String type;
    private boolean customizable;
    private CustomizedRule customRule;

    private Rule(int id, String name, String description) {
        this.id = id;
        this.name = name;

        this.description = description;
    }

    private Rule(int id, String name, String description, boolean customizable) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.customizable = customizable;
    }

    private Rule(int id, String name, String type, String description) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.description = description;
    }

    public CustomizedRule getCustomRule() {
        return customRule;
    }

    public void setCustomRule(CustomizedRule customRule) {
        this.customRule = customRule;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isCustomizable() {
        return customizable;
    }

    public void setCustomizable(boolean customizable) {
        this.customizable = customizable;
    }

    @Override
    public String toString() {
        return name;
    }

    public static List<Rule> getRules() {
        List<Rule> ruleList = new ArrayList<>(Arrays.asList(Rule.values()));
        return ruleList;
    }

    public static Rule getRuleById(int id) {
        for (Rule r : values()) {
            if (r.getId() == id) {
                return r;
            }
        }

        return null;
    }

    public abstract boolean check(Data d);

    public abstract boolean check(Data d, CustomizedRule cr);

    public abstract boolean check(Data d, List<GenericPojo> list);
}
