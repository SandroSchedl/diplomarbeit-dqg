/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto.enums;

import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.Data;
import dataqualitygateway.infrastructure.parser.ParserException;
import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.bson.BsonDocument;
import org.bson.BsonDouble;
import org.bson.BsonElement;
import org.bson.BsonInt32;
import org.bson.BsonInt64;
import org.bson.BsonNull;
import org.bson.BsonString;
import org.bson.BsonValue;

/**
 *
 * @author SandroSchedl
 */
@ManagedBean
@SessionScoped
public enum DataType {
    STRING(String.class, "String") {
        @Override
        public Object parse(String value) {
            return value;
        }
        
        @Override
        public BsonValue getBsonValue(Data d) {
            if(d.getValue() == null)    return new BsonNull();
            
            return new BsonString(d.getValue().toString());
        }
        
    },
    INTEGER(Integer.class, "Integer") {

        @Override
        public Object parse(String value) throws ParserException {
            try{
              return   Integer.parseInt(value);
            }catch(NumberFormatException e){
                throw new ParserException("DataType INTEGER: Value could not be parsed!" + value, e);
            }
        }
        
        @Override
        public BsonValue getBsonValue(Data d) {
            if(d.getValue() == null)    return new BsonNull();
            
            return new BsonInt32( new Integer(d.getValue().toString()) );
        }
        
    },
    DOUBLE(Double.class, "Double") {

        @Override
        public Object parse(String value) throws ParserException {
            if(value == null)   throw new ParserException("DataType Double: Value is null!");
            
            try{
              return Double.parseDouble(value);     //If the value is null it would throw a NullPointerException
            }catch(NumberFormatException e){
                throw new ParserException("DataType Double: Value could not be parsed!" + value, e);
            }
        }
        
        @Override
        public BsonValue getBsonValue(Data d) {
            if(d.getValue() == null)    return new BsonNull();
            
            return new BsonDouble( new Double(d.getValue().toString()) );
        }
        
    },
    NULL(null,"Null"){
        @Override
        public Object parse(String value) throws ParserException {
            return null;
        }

        @Override
        public BsonValue getBsonValue(Data d) {
            return new BsonNull();
        }

    };

    private String name;
    private Class type;

    private DataType(Class type, String name) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public Class getType() {
        return type;
    }

    public abstract Object parse(String value) throws ParserException;
    public abstract BsonValue getBsonValue(Data d);

    public static List<DataType> getDataTypes() {
        return Arrays.asList(DataType.values());
    }
    
    public static DataType getTypeOfValue(Class type){
        for(DataType t : DataType.values()){
            if(t.getType() == type)
                return t;
        }
        
        return DataType.STRING;
    }



}
