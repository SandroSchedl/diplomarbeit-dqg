/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import org.bson.BsonString;
import org.bson.BsonValue;

/**
 *
 * @author SandroSchedl
 */
public class Result implements Serializable{
    private String resultId; 
    private List<GenericPojo> recordList;
    private long time;
    private Template template;
    private String fileName;
    private LocalDateTime date;
    private int recordCnt;
    private int anzCorrectRecords;
    private int anzRecordsWithOneError;
    private int anzRecordsWithMoreErrors;

    public Result() {
    }

    public Result(Template t, String fileName) {
        this(t,fileName,0,0,0,0,true);
    }

    public Result(Result currentResult) {
        this(currentResult.getTemplate(),
                currentResult.getFileName(),
                currentResult.getTime(),
                currentResult.getAnzCorrectRecords(),
                currentResult.getAnzRecordsWithOneError(),
                currentResult.getAnzRecordsWithMoreErrors(),
                false);
        this.recordList = new LinkedList<>(currentResult.getRecordList());
        this.resultId = currentResult.getResultId();
        this.template = new Template(this.getTemplate());
    }
    
    public String getFormattedTimeStamp(){
        if(date != null){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH:mm:ss");
        return date.format(dtf);
        }
        return "";
    }
    
    

    public Result(Template template, String fileName, long time, int anzCorrectRecords, int anzRecordsWithOneError, int anzRecordsWithMoreErrors, boolean setResultId) {
        this.time = time;
        this.template = template;
        this.fileName = fileName;
        this.anzCorrectRecords = anzCorrectRecords;
        this.anzRecordsWithOneError = anzRecordsWithOneError;
        this.anzRecordsWithMoreErrors = anzRecordsWithMoreErrors;
        this.date = LocalDateTime.now();
        if(setResultId)
            resultId = template.getName() + "temp_" + template.increaseAndGetLastId();
        System.out.println("TEMPLATE ID: " + template.getLastId());
        recordList = new LinkedList<>();
    }
    
        
    public BsonValue getResultIdAsBsonValue() {
        return new BsonString(resultId);
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
    
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    public int getPercentageOfAll(int anz){
        return anz * 100 / (this.anzCorrectRecords + this.anzRecordsWithMoreErrors + this.anzRecordsWithOneError );
    }

    public int getAnzCorrectRecords() {
        return anzCorrectRecords;
    }

    public void setAnzCorrectRecords(int anzCorrectRecords) {
        this.anzCorrectRecords = anzCorrectRecords;
    }

    public int getAnzRecordsWithOneError() {
        return anzRecordsWithOneError;
    }

    public void setAnzRecordsWithOneError(int anzRecordsWithOneError) {
        this.anzRecordsWithOneError = anzRecordsWithOneError;
    }

    public int getAnzRecordsWithMoreErrors() {
        return anzRecordsWithMoreErrors;
    }

    public void setAnzRecordsWithMoreErrors(int anzRecordsWithMoreErrors) {
        this.anzRecordsWithMoreErrors = anzRecordsWithMoreErrors;
    }

    public List<GenericPojo> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<GenericPojo> recordList) {
        this.recordList = recordList;
    }

    public int getRecordCnt() {
        return recordCnt;
    }
    
    public int increaseAndGetRecordCnt(){
        return ++recordCnt;
    }

    public void setRecordCnt(int recordCnt) {
        this.recordCnt = recordCnt;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public Template getTemplate() {
        return template;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

    public String getResultId() {
        return resultId;
    }

    public void setResultId(String resultId) {
        this.resultId = resultId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Result other = (Result) obj;
        
        return Objects.equals(this.resultId, other.resultId);
    }

    public void setStatistics(long time, int perf, int oneWrong, int wrong) {
        this.time = time;
        this.anzCorrectRecords = perf;
        this.anzRecordsWithOneError = oneWrong;
        this.anzRecordsWithMoreErrors = wrong;
    }



    
}
