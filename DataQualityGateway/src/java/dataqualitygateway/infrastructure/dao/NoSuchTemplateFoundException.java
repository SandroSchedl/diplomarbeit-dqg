/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.infrastructure.dao;

/**
 *
 * @author SandroSchedl
 */
public class NoSuchTemplateFoundException extends Exception {

    public NoSuchTemplateFoundException(String msg) {
        super(msg);
    }
    
}
