/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.infrastructure.dao;

import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.Data;
import dataqualitygateway.dto.GenericPojo;
import dataqualitygateway.dto.Result;
import dataqualitygateway.dto.enums.Rule;
import dataqualitygateway.infrastructure.dao.connection.MySQLConnectionManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SandroSchedl
 */
public class MySQLDao {

    private MongoDBDao mongoDao;
    private Semaphore sem;

    private Result result;
    private final String DB_NAME;
    private final String DB_VALUES;
    private final int DB_COLUMN_COUNT;

    private static final String DB_RULE_NAME = "rulecolumn";
    private static final String DB_RULE_VALUES = "values (?,?,?,?)";

    public MySQLDao(Result result, MongoDBDao db) {
        this.mongoDao = db;
        this.result = result;
        this.sem = new Semaphore(10);

        DB_NAME = result.getTemplate().getName().toLowerCase();
        DB_COLUMN_COUNT = result.getTemplate().getColumnList().size();

        int i;
        StringBuilder sb = new StringBuilder("values(");
        for (i = 0; i < DB_COLUMN_COUNT + 1; i++) {       //+1... all the columns of the record itself and additionally the recordId
            sb.append("?,");
        }

        i = sb.lastIndexOf(",");
        sb.replace(i, i + 1, ")");

        DB_VALUES = sb.toString();
    }

    public void writeRecords() {

//        long startTime, endTime;
//        startTime = System.nanoTime();
//        CountDownLatch latch = new CountDownLatch(result.getRecordCnt());
        int i;
        String recId;

        try (Connection conn = MySQLConnectionManager.getInstance().getConnection();
                PreparedStatement recordStatement = conn.prepareStatement("insert into " + DB_NAME + ' ' + DB_VALUES);
                PreparedStatement ruleStatement = conn.prepareStatement("insert into " + DB_RULE_NAME + ' ' + DB_RULE_VALUES);
                ) {

            for (GenericPojo record : result.getRecordList()) {
                i = 1;
                recId = record.getRecordId();

                recordStatement.setString(i++, recId);
//                new Thread(() -> {
//                    mongoDao.writeRules(record);
//                    latch.countDown();
//                }).start();
                for (Entry<Column, Data> entry : record.getValueMap().entrySet()) {

                    for (Entry<Rule, Boolean> e : entry.getValue().getRuleMap().entrySet()) {
                        try {

                            ruleStatement.setString(1, record.getRecordId());
                            ruleStatement.setString(2, entry.getKey().getName());
                            ruleStatement.setInt(3, e.getKey().getId());
                            ruleStatement.setBoolean(4, e.getValue());

                            ruleStatement.executeUpdate();
                        } catch (SQLException ex) {
                            Logger.getLogger(MySQLDao.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    recordStatement.setObject(i++, entry.getValue().getValue());
                }

                recordStatement.executeUpdate();

            }

//            latch.await();
//            endTime = System.nanoTime() - startTime;
//            endTime = TimeUnit.MILLISECONDS.convert(endTime, TimeUnit.NANOSECONDS);

        } catch (SQLException ex){//| InterruptedException ex) {
            Logger.getLogger(MySQLDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<GenericPojo> readRecordsByResult() {
        long startTime, endTime;
        startTime = System.nanoTime();
        List<GenericPojo> records = new LinkedList<>();
        List<Column> columns = result.getTemplate().getColumnList();
        GenericPojo temp;
        Data d;
        Column c;
        int i;

        try (Connection conn = MySQLConnectionManager.getInstance().getConnection();
                PreparedStatement recordStatement = conn.prepareStatement("select * from " + DB_NAME);
                PreparedStatement ruleStatement = conn.prepareStatement("select * from rulecolumn where recordId = ? and columnName = ?");
                ResultSet set = recordStatement.executeQuery(); ){
            
            while(set.next()){
                temp = new GenericPojo();
                temp.setRecordId(set.getString(1));
                ruleStatement.setString(1, temp.getRecordId());
                
                for(i=1; i < columns.size()+1; i++){
                    d = new Data(set.getObject(i+1));
                    c = columns.get(i-1);
                    
                    ruleStatement.setString(2, c.getName());
                    d.setRuleMap(readRules(ruleStatement));
                    temp.addValue(c, d);
                }
                
                records.add(temp);
                
            }

        } catch (SQLException ex) {
            Logger.getLogger(MySQLDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        endTime = System.nanoTime() - startTime;
        endTime = TimeUnit.MILLISECONDS.convert(endTime, TimeUnit.NANOSECONDS);
        System.out.println("\n\n\n\n\n\n "+ endTime +" size: " + records.size() + "\n\n\n\n\n\n");
        return records;
    }

    private Map<Rule, Boolean> readRules(PreparedStatement ruleStatement) {
        Map<Rule,Boolean> retVal = new HashMap<>();
        
        try(ResultSet set = ruleStatement.executeQuery()){
            
            while(set.next())
                retVal.put(Rule.getRuleById(set.getInt("ruleId")), set.getBoolean("status"));
            
        } catch (SQLException ex) {
            Logger.getLogger(MySQLDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return retVal;
    }

}
