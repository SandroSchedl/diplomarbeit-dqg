/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.infrastructure.dao.connection;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author SandroSchedl
 */
public class DQGProperties {
    private static volatile DQGProperties compProp = new DQGProperties();
    private Properties prop;
    
    public final static String DB_URL = "db.url";
    public final static String URL_SETTINGS = "url.settings";
    public final static String DB_USER= "db.user";
    public final static String DB_PW = "db.pw";
    public final static String DB_SRV = "db.srv";
    public final static String DB_NAME = "DataQualityGateway";
    public final static String DB_MAXCONN = "db.maxConn";

    private DQGProperties(){
        prop = new Properties();
        
        try {
            prop.load(getClass().getResourceAsStream("DQGProperties.properties"));
        } catch (IOException ex) {
            //Logger Ausgabe
        }
        
    }    
    
    public static synchronized DQGProperties getInstance(){
        return compProp;
    }
    
    public String getStringProperty(String propertyName){            
        return prop.getProperty(propertyName, "???");
    }
    
    public int getIntProperty(String propertyName){
        return Integer.parseInt( prop.getProperty(propertyName, "-1") );
    }
}
