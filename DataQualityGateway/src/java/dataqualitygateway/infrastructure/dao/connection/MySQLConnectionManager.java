/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.infrastructure.dao.connection;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.sql.PooledConnection;

/**
 *
 * @author SandroSchedl
 */
public class MySQLConnectionManager {

    private MysqlDataSource dataSource;
    private static MySQLConnectionManager cm = new MySQLConnectionManager();

    private MySQLConnectionManager() {

        dataSource = new MysqlConnectionPoolDataSource();
        dataSource.setUser("admin");
        dataSource.setPassword("admin");
        dataSource.setServerName("localhost");
        dataSource.setDatabaseName("dqg");
        dataSource.setPortNumber(3306);
        
        try {
            dataSource.setAllowMultiQueries(true);
        } catch (SQLException ex) {
            Logger.getLogger(MySQLConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        dataSource.setURL("jdbc:mysql://localhost:3306/dqg?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
    }

    public synchronized Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public static MySQLConnectionManager getInstance() {
        return cm;
    }
    
}
