/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.infrastructure.dao.connection;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

/**
 *
 * @author SandroSchedl
 */
public class MongoDBConnectionManager {

    private static final MongoDBConnectionManager CM = new MongoDBConnectionManager();
    private MongoDatabase database;

    private MongoDBConnectionManager() {
        DQGProperties prop = DQGProperties.getInstance();
        
        StringBuilder sb = new StringBuilder();
        sb.append(prop.getStringProperty(DQGProperties.DB_SRV));
        sb.append(prop.getStringProperty(DQGProperties.DB_USER));
        sb.append(":");
        sb.append(prop.getStringProperty(DQGProperties.DB_PW));
        sb.append(prop.getStringProperty(DQGProperties.DB_URL));
        sb.append(prop.getStringProperty(DQGProperties.URL_SETTINGS));
        
        MongoClientURI uri = new MongoClientURI(sb.toString());
        this.database = new MongoClient(uri).getDatabase(DQGProperties.DB_NAME);
    }

    public static synchronized MongoDBConnectionManager getInstance() {
        return CM;
    }

    public MongoDatabase getDatabase() {
        return database;
    }
}
