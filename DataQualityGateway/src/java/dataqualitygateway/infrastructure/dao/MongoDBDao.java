/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.infrastructure.dao;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.CustomizedRule;
import dataqualitygateway.dto.Data;
import dataqualitygateway.dto.GenericPojo;
import dataqualitygateway.dto.Group;
import dataqualitygateway.dto.Result;
import dataqualitygateway.dto.Template;
import dataqualitygateway.dto.enums.DataType;
import dataqualitygateway.dto.enums.Rule;
import dataqualitygateway.infrastructure.dao.connection.DQGProperties;
import dataqualitygateway.infrastructure.dao.connection.MongoDBConnectionManager;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.BsonDocument;
import org.bson.Document;

/**
 *
 * @author SandroSchedl
 */
public class MongoDBDao implements Serializable {

    private MongoDatabase db;
    private Semaphore sem;

    public MongoDBDao() {
        db = MongoDBConnectionManager.getInstance().getDatabase();
        sem = new Semaphore(DQGProperties.getInstance().getIntProperty(DQGProperties.DB_MAXCONN));
    }

    public void writeResult(Result r) throws InterruptedException {
        MongoCollection<Document> doc = db.getCollection("Results");

        doc.insertOne(new Document("_id", r.getResultId())
                .append("template", r.getTemplate().getName())
                .append("recordCnt", r.getRecordCnt())
                .append("correctRecords", r.getAnzCorrectRecords())
                .append("oneErrorRecords", r.getAnzRecordsWithOneError())
                .append("incorrectRecords", r.getAnzRecordsWithMoreErrors())
                .append("fileName", r.getFileName())
                .append("date", r.getDate()));

    }

    public List<Result> readResultsByTemplate(Template t) {
        MongoCollection<Document> doc = db.getCollection("Results");
        System.out.println("READYBYTEMPLATE\n");
        List<Result> retVal = new ArrayList<>();
        Result r;

        for (Document d : doc.find(new BasicDBObject("_id", new BasicDBObject("$regex", t.getName() + "_")))) {       // + "_" so it is definetly the end of the templatename
            r = readResult(d, t);

            retVal.add(r);
        }
        System.out.println("endReadByTemplate");

        return retVal;
    }

    public List<Result> readLastResults() throws NoSuchTemplateFoundException {
        MongoCollection<Document> doc = db.getCollection("Results");

        List<Result> retVal = new ArrayList<>();
        Result r;

        for (Document d : doc.find()) {
            r = readResult(d);
            if (r != null) {
                retVal.add(r);
            }
        }

        return retVal;
    }

    private Result readResult(Document d, Template t) {
        Result r = new Result();
        r.setResultId(d.getString("_id"));
        r.setRecordCnt(d.getInteger("recordCnt"));
        r.setAnzCorrectRecords(d.getInteger("correctRecords"));
        r.setAnzRecordsWithOneError(d.getInteger("oneErrorRecords"));
        r.setAnzRecordsWithMoreErrors(d.getInteger("incorrectRecords"));
        r.setFileName(d.getString("fileName"));
        r.setDate(d.getDate("date").toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());

        r.setTemplate(t);
        r.setRecordList(readRecordsByResult(r));
        return r;
    }

    private Result readResult(Document d) throws NoSuchTemplateFoundException {
        String tempName = d.getString("template");

        for (Template t : readTemplates(false)) {
            if (t.getName().equals(tempName)) {
                t.setColumnList(readColumns(t));
                return readResult(d, t);
            }
        }

        return null;
    }

    public Set<Template> readTemplates(boolean readColumns) {
        MongoCollection<Document> doc = db.getCollection("Templates");

        Set<Template> retVal = new HashSet();
        Template t;

        for (Document d : doc.find()) {
            t = new Template(d.getString("_id"));
            t.setLastId(d.getInteger("lastId"));

            if (readColumns) {
                t.setColumnList(readColumns(d));
            }

            retVal.add(t);
        }

        return retVal;
    }

    public void writeTemplate(Template t) {
        MongoCollection<Document> doc = db.getCollection("Templates");

        doc.insertOne(new Document("_id", t.getName()).append("lastId", t.getLastId())
                .append("columns", new BsonDocument(t.getColumnsAsBsonObject()))
                .append("results", t.getResultsAsBsonArray()));
    }

    public void updateTemplate(Template t, boolean updateColumns) {
        MongoCollection<Document> doc = db.getCollection("Templates");

        if (updateColumns) {
            doc.updateOne(Filters.eq("_id", t.getName()),
                    new BasicDBObject("$set",
                            new Document("lastId", t.getLastId())
                                    .append("results", t.getResultsAsBsonArray())
                                    .append("columns", new BsonDocument(t.getColumnsAsBsonObject()))));
        } else {
            doc.updateOne(Filters.eq("_id", t.getName()),
                    new BasicDBObject("$set",
                            new Document("lastId", t.getLastId())
                                    .append("results", t.getResultsAsBsonArray())));
        }
    }

    private List<Column> readColumns(Document d) {
        List<Column> retVal = new ArrayList<>();

        Document columns = d.get("columns", Document.class);
        Document col;
        Column c;

        for (Entry<String, Object> e : columns.entrySet()) {
            col = (Document) e.getValue();

            c = new Column(e.getKey());
            c.setDataType(DataType.valueOf(col.get("dataType").toString().toUpperCase()));
            c.setRules(col.get("ruleList", ArrayList.class));

            Object cR = col.get("customRule");
            if (cR == null) {
                c.setCustomRule(null);
            } else {
                Document dR = (Document) cR;

                c.setCustomRule(new CustomizedRule(dR.getInteger("min"), dR.getInteger("max"),
                        dR.getString("country"), dR.getString("bannedWords"), dR.getString("DateFormat"), dR.getString("parameter")));
            }
            retVal.add(c);
        }

        return retVal;
    }

    public List<Column> readColumns(Template t) {
        MongoCollection<Document> doc = db.getCollection("Templates");

        for (Document d : doc.find(Filters.eq("_id", t.getName()))) {
            return readColumns(d);      //template-name is the PK therefore the loop would only iterate through once
        }

        return null;
    }

    public List<GenericPojo> readRecordsByResult(Result r) {
        long startTime, endTime;
        startTime = System.nanoTime();

        MongoCollection<Document> doc = db.getCollection("Records");

        List<GenericPojo> retVal = new ArrayList<>();
        List<Column> columnList = r.getTemplate().getColumnList();
        GenericPojo p;
        Column c;
        Data d;
        Document rec, valuesAndRules, rules;

        //finds all records whose id starts with the according template-name
        for (Document document : doc.find(new BasicDBObject("_id", new BasicDBObject("$regex", r.getTemplate().getName() + "_")))) {         // + "_" so it is definetly the end of the templatename

            //reads all the information from the record
            rec = document.get("record", Document.class);
            p = new GenericPojo();

            p.setRecordId(document.get("_id", String.class));

            //iterates through all fields in the record
            for (int i = 0; i < rec.size(); i++) {
                c = columnList.get(i);  //gets a column from the columnList from the template parameter

                valuesAndRules = rec.get(c.getName(), Document.class);      //gets the value and the rules from the same-named column
                d = new Data(valuesAndRules.get("value"));                  //gets the value from the column

                rules = valuesAndRules.get("rules", Document.class);        //gets the rules from the column

                for (Entry<String, Object> e : rules.entrySet()) {             //iterates through all rules and adds them to the data object
                    d.addRule(Rule.getRuleById(Integer.parseInt(e.getKey())), (Boolean) e.getValue());
                }
                p.addValue(c, d);       //adds the data object and the column to the record

            }

            retVal.add(p);
        }

        endTime = System.nanoTime() - startTime;
        endTime = TimeUnit.MILLISECONDS.convert(endTime, TimeUnit.NANOSECONDS);

        return retVal;
    }

    public void writeRecords(Result r) throws InterruptedException {
        System.out.println("WRTIERECORDS\n");
        MongoCollection<Document> doc = db.getCollection("Records");
        CountDownLatch latch = new CountDownLatch(r.getRecordCnt());

        for (GenericPojo p : r.getRecordList()) {

            new Thread(() -> {
                try {
                    sem.acquire();
                    doc.insertOne(new Document("_id", p.getRecordId()).append("record", p.getRecordAsBsonObject()));
                } catch (InterruptedException ex) {
                    Logger.getLogger(MongoDBDao.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    sem.release();
                    latch.countDown();
                }
            }).start();
        }
        latch.await();
    }

    @Deprecated
    public void writeRules(GenericPojo p) {
        try {
            sem.acquire();
            MongoCollection<Document> doc = db.getCollection("RuleResult");
//            long startTime, endTime;
//            startTime = System.nanoTime();
            //        resultRecordCntLock.lock();
            doc.insertOne(new Document(p.getRecordId(), p.getRecordAsBsonObjectWithoutValues()));
//            endTime = System.nanoTime() - startTime;
//            endTime = TimeUnit.MILLISECONDS.convert(endTime, TimeUnit.NANOSECONDS);

        } catch (InterruptedException ex) {
            Logger.getLogger(MongoDBDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            sem.release();
        }
    }

    public void writeGroup(Group g) {
        MongoCollection<Document> doc = db.getCollection("Groups");

        doc.insertOne(g.getGroupAsBsonDocument());
    }

    public void deleteGroup(Group g) {
        MongoCollection<Document> doc = db.getCollection("Groups");

        doc.deleteOne(Filters.eq("_id", g.getGroupId()));
    }

    public List<Group> readGroups() {
        MongoCollection<Document> doc = db.getCollection("Groups");

        List<Group> groupList = new ArrayList<>();
        Group g;

        for (Document d : doc.find()) {
            g = new Group(d.getInteger("_id"));
            g.setGroupName(d.getString("name"));
            g.setRules(d.get("ruleList", ArrayList.class));

            groupList.add(g);
        }

        return groupList;
    }
}
