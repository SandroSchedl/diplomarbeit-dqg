/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.infrastructure.parser;

/**
 *
 * @author SandroSchedl
 */
public class ParserException extends Exception{

    public ParserException(String msg, Throwable thr) {
        super(msg, thr);
    }

    public ParserException(String msg) {
        super(msg);
    }
    
    
}
