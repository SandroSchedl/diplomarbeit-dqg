/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.infrastructure.parser;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import static java.util.logging.Logger.*;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Admin-PC
 */
public abstract class ParserFactory {

    public static Parser getParser(FileUploadEvent event) {
        try {
            String path = event.getFile().getFileName();
            
            switch (path.substring(path.lastIndexOf("."), path.length())) {
                case ".json":
                    return new JSONParserr(event.getFile().getInputstream());
                case ".csv":
                    return new CSVParser(event.getFile().getInputstream());
                case ".xml":
                    return new XMLParser(event.getFile().getInputstream());
            }
        } catch (IOException ex) {
           getLogger(ParserFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
  
}
