/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.infrastructure.parser;

import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.GenericPojo;
import dataqualitygateway.dto.Data;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Admin-PC
 */
public class CSVParser extends Parser {

    private String splitBy = ",";

    public CSVParser(InputStream s) {
        super(s);
    }

    /*Method to convert first line in csv to columns*/
    @Override
    public void parseColumns() {
        String line;
        String[] data;

        try {
            if ((line = getReader().readLine()) != null) {
                //column String gets splitted into the individual columns
                data = line.trim().split(splitBy);
                
                //if Column is empty then it can be the case that "ï»¿" is in it instead of ""
                //if Column name is empty then it should be converted to "id"
                if (data.length > 0 && (data[0].contains("ï»¿") || data[0].equals(""))) {
                    data[0] = "id";
                }
                
                //add columns to columnList
                for(int i=0; i<data.length; i++)
                    columns.add(new Column(data[i]));
            }

        } catch (IOException ex) {
            Logger.getLogger(CSVParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*Method to convert other lines into GenericPojos*/
    @Override
    public void parseRecords(List<Column> columnList) {
        BufferedReader br = getReader();
        String line = "";

        try {
            String[] data;
            String help;
            GenericPojo gPojo;
            Object obj;
            Column c;

            while ((line = br.readLine()) != null) {
                //record String gets splitted into single columns
                data = line.trim().split(splitBy);
                help = "";

                gPojo = new GenericPojo();

                for (int i = 0, j = 0; i < data.length; i++, j++) {
                    c = columnList.get(j);
                    //check if the record has sentences with double apostrophe in it
                    //if such a sentence has a comma in it then a problem would occur while reading
                    //creates a new string without double apostrophe
                    if (data[i].startsWith("\"")) {
                        while (!data[i].endsWith("\"")) {
                            help += data[i++];
                            help += ",";
                        }

                        help += data[i];
                        //remove double apostrophe of sentence
                        help = help.replaceAll("\"", "");

                        obj=null;
                        try {
                            //parse value of specific Colum (c) into selected datatype
                            obj = c.getDataType().parse(help);
                        } catch (ParserException ex) {
                            Logger.getLogger(CSVParser.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        //add parsed value to pojo
                        gPojo.addValue(c, new Data(obj));
                        help = "";

                    } else {
                        try {
                            //set value of specific column null if empty
                            if (data[i].equals("")) {
                                data[i] = null;
                            }

                            obj = c.getDataType().parse(data[i]);
                        } catch (ParserException ex) {
                            //Datenprüfung mitschreiben?
                            obj = data[i];
                        }
                        
                    }
                    //add parsed value to pojo
                    gPojo.addValue(c, new Data(obj));
                }
                
                //only add pojo to recordList if it has the right amount of columns
                if (gPojo.getValueMap().size() == columns.size()) {
                    records.add(gPojo);
                } else {
                    FacesContext fc = FacesContext.getCurrentInstance();
                    
                    if (fc != null) {
                        String[] id = line.split(splitBy);
                        fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, bundle.getString("ParserIncorrectColumCount") + id[0], null));
                    }

                }

            }

        } catch (IOException ex) {
            Logger.getLogger(CSVParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ArrayIndexOutOfBoundsException e) {
            FacesContext fc = FacesContext.getCurrentInstance();
            String[] id = line.split(splitBy);
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, bundle.getString("ParserEnterWithinRecord") + id[0], null));
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
