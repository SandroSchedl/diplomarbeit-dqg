/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.infrastructure.parser;

import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.GenericPojo;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import org.apache.poi.util.IOUtils;

/**
 *
 * @author Admin-PC
 */
public abstract class Parser {

    protected List<GenericPojo> records = new ArrayList<>();
    protected List<Column> columns = new ArrayList<>();
    protected ResourceBundle bundle;

    private BufferedReader reader;
    private byte[] bytes;

    public Parser(InputStream s) {
        
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            IOUtils.copy(s, baos);
            bytes = baos.toByteArray();
        } catch (IOException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }

        initReader();

        FacesContext ctx = FacesContext.getCurrentInstance();
        Application app = ctx.getApplication();
        bundle = app.getResourceBundle(ctx, "bundleVar");
    }

    private void initReader() {
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        reader = new BufferedReader(new InputStreamReader(bais));
    }
    
    public List<Column> getColumns() {
        columns.clear();
        parseColumns();

        return columns;
    }

    public List<GenericPojo> getRecords(List<Column> columnList) {

        initReader();
        getColumns();

        records.clear();
        parseRecords(columnList);
        return records;
    }
    
    //parses only first record in file to get the columns
    public abstract void parseColumns();
       
    //parses all records of file
    //columnList is needed to parse specific columns of a record into the right datatype
    public abstract void parseRecords(List<Column> columnList);

    public BufferedReader getReader() {
        return reader;
    }

    public InputStream getStream() {
        return new ByteArrayInputStream(bytes);
    }

}
