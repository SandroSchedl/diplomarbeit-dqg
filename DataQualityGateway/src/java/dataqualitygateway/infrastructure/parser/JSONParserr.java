/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.infrastructure.parser;

import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.Data;
import dataqualitygateway.dto.GenericPojo;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Admin-PC
 */
public class JSONParserr extends Parser {

    private JSONArray array = null;

    public JSONParserr(InputStream s) {
        super(s);
    }
    
    /*Method to convert first line in json to columns*/
    @Override
    public void parseColumns() {
        JSONParser parser = new JSONParser();

        try {
            if (array == null) 
                array = (JSONArray) parser.parse(getReader());
            
            Object o;
            GenericPojo gPojo;

            //read first element in JSONFile to get columns
            if (array.size() > 0) {
                o = array.get(0);

                JSONObject genericPojo = (JSONObject) o;
                gPojo = new GenericPojo();

                for (Object key : genericPojo.keySet()) {
                    columns.add(new Column((String) key));
                }
            }

        } catch (ParseException ex) {
            FacesContext fc = FacesContext.getCurrentInstance();
            if (fc != null) {
                String jsonMsg = bundle.getString("IncorrectJSONFormat");
                MessageFormat msgFmt = new MessageFormat(jsonMsg);
                jsonMsg = msgFmt.format("" + ex.getPosition(), ex.getMessage(), ex.getUnexpectedObject());
                
                fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, jsonMsg, null));
            }
        } catch (Exception ex) {
            Logger.getLogger(JSONParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /*Method to convert other lines into GenericPojos*/
    @Override
    public void parseRecords(List<Column> columnList) {
        JSONParser parser = new JSONParser();

        try {
            if (array == null) {
                array = (JSONArray) parser.parse(getReader());
            }

            Object o;
            GenericPojo gPojo;
            Object value;
            Object obj;

            //read all elements of jsonFile
            for (int i = 0; i < array.size(); i++) {
                o = array.get(i);

                JSONObject genericPojo = (JSONObject) o;
                gPojo = new GenericPojo();

                //fill generic pojo with data             
                for (Column key : columnList) {
                    if (genericPojo.containsKey((key).getName())) {
                        value = genericPojo.get((key).getName());
                    }else{
                        value = null;
                    }

                    try {
                        if(value != null)
                            obj = key.getDataType().parse(value.toString());
                        else
                            obj = null;
                        
                    } catch (ParserException ex) {
                        obj = null;
                    }
                    gPojo.addValue(key, new Data(obj));
                }

                records.add(gPojo);
            }

        }catch (IOException ex) {
            Logger.getLogger(JSONParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            FacesContext fc = FacesContext.getCurrentInstance();
            String jsonMsg = bundle.getString("IncorrectJSONFormat");
            MessageFormat msgFmt = new MessageFormat(jsonMsg);
            jsonMsg = msgFmt.format("" + ex.getPosition(), ex.getMessage(), ex.getUnexpectedObject());
          
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, jsonMsg, null));

        }

    }

}
