/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.infrastructure.parser;

import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.Data;
import dataqualitygateway.dto.GenericPojo;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Admin-PC
 */
public class XMLParser extends Parser {

    private NodeList elementsOfDoc = null;

    public XMLParser(InputStream s) {
        super(s);
    }

    public void parseDocument() throws ParserConfigurationException, SAXException, IOException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        factory.setIgnoringElementContentWhitespace(true);
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document doc = builder.parse(getStream());
        elementsOfDoc = doc.getChildNodes();

    }

    //liest alle Datensätze raus
    //falls onlyColumns auf true gesetzt ist, wird nur das 1. Element zur Spaltensetzung rausgelesen
    public void readDataFromDoc(NodeList nodeList, boolean onlyColumns, List<Column> columnList) {
        Node child, child2, child3, child4;
        GenericPojo gPojo = null;

        try {
            for (int i = 0; i < nodeList.getLength(); i++) {
                //rootElement
                child = nodeList.item(i);

                for (int j = 0; child.getChildNodes() != null && j < child.getChildNodes().getLength(); j++) {
                    child2 = child.getChildNodes().item(j);
                    gPojo = new GenericPojo();
                   
                    if(onlyColumns && child2.getChildNodes().getLength() == 0)
                        continue;
                    //read 1 record
                    getDataFromRecord(child2, gPojo, onlyColumns, columnList);
                    
                    
                    //Falls nicht alle Spalten im Record im XML angegeben, werden diese mit null befüllt
                    if (columnList != null && gPojo.getValueMap().size() > 0 &&  gPojo.getValueMap().size()< columnList.size()) {
                        for (Column key : columnList) {
                            if (!gPojo.getValueMap().containsKey(key)) 
                                gPojo.addValue(key, new Data(null));
//                            }else{
//                                gPojo.addValue(key, new Data(key.getDataType().parse(gPojo.getValueMap().get(key).getValue().toString())));
//                            }
                        }
                    }

                    if (gPojo.getValueMap().size() > 0) {
                        records.add(gPojo);
                    }
                    
                    if (onlyColumns) {
                        return;
                    }

                }
            }
        } catch (Exception e) {
            FacesContext fc = FacesContext.getCurrentInstance();
            if (fc != null) {
                fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, bundle.getString("IncorrectXMLFormat"), null));
            }
        }

    }

    //liest 1 Datensatz aus dem XML raus
    public GenericPojo getDataFromRecord(Node record, GenericPojo gPojo, boolean onlyColumns, List<Column> columnList){
        Node children, child;
        String nodeName;
        String nodeValue;

        for (int k = 0; record != null && k < record.getChildNodes().getLength(); k++) {
            children = record.getChildNodes().item(k);
            if(onlyColumns && children.getChildNodes().getLength() == 0 )
                continue;
            for (int l = 0; children != null && l < children.getChildNodes().getLength(); l++) {
               
                    child = children.getChildNodes().item(l);
                    nodeName = child.getParentNode().getNodeName();
                    nodeValue = child.getTextContent();
                    
                    //first check -> only columns are read -> columnList = null
                    if(columnList != null){
                    Column c = null;
                    for(Column co : columnList){
                        if(co.getName().equalsIgnoreCase(nodeName)){
                            c = co;
                            break;
                        }
                    }
                    
                try {
                    //add and parse Value of Column of current Record
                    gPojo.addValue(c, new Data(c.getDataType().parse(nodeValue)));
                } catch (ParserException ex) {
                    Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
                }
                }else{
                   //if first check then add name of columns without parsing to create columnlist
                   gPojo.addValue(new Column(nodeName), new Data(nodeValue)); 
                }
            }
        }
        return gPojo;
    }
    
    /*Method to convert first line in json to columns*/
    @Override
    public void parseColumns() {
        try {
            parseDocument();

            if (elementsOfDoc != null) {

                readDataFromDoc(elementsOfDoc, true,null);      
                
                //read columns from first record
                if (records != null && records.size() > 0) {
                    for (Object key : records.get(0).getValueMap().keySet()) 
                        columns.add((Column) key);
                                       
                    records.clear();
                }
            }

        } catch (ParserConfigurationException | IOException | org.xml.sax.SAXException ex) {
            Logger.getLogger(XMLParser.class
                    .getName()).log(Level.SEVERE, null, ex);
            FacesContext fc = FacesContext.getCurrentInstance();
            if (fc != null) {
                fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getMessage(), null));
            }
        }

    }
    
    /*Method to convert other lines into GenericPojos*/
    @Override
    public void parseRecords(List<Column> columnList) {
        if (elementsOfDoc != null) {
            readDataFromDoc(elementsOfDoc, false,columnList);
        }
    }
    
}
/*

package dataqualitygateway.infrastructure.parser;

import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.Data;
import dataqualitygateway.dto.GenericPojo;
import dataqualitygateway.dto.enums.DataType;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class XMLParser extends Parser {

    private NodeList elementsOfDoc = null;

    public XMLParser(InputStream s) {
        super(s);
    }

    public void parseDocument() throws ParserConfigurationException, SAXException, IOException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        factory.setIgnoringElementContentWhitespace(true);
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document doc = builder.parse(getStream());
        elementsOfDoc = doc.getChildNodes();

    }

    //liest alle Datensätze raus
    //falls onlyColumns auf true gesetzt ist, wird nur das 1. Element zur Spaltensetzung rausgelesen
    public void readDataFromDoc(NodeList nodeList, boolean onlyColumns, List<Column> columnList) {
        Node child, child2, child3, child4;
        GenericPojo gPojo = null;

        try {
            for (int i = 0; i < nodeList.getLength(); i++) {
                //rootElement
                child = nodeList.item(i);

                for (int j = 0; child.getChildNodes() != null && j < child.getChildNodes().getLength(); j++) {
                    child2 = child.getChildNodes().item(j);
                    gPojo = new GenericPojo();
                   
                    if(onlyColumns && child2.getChildNodes().getLength() == 0)
                        continue;
                    //read 1 record
                    getDataFromRecord(child2, gPojo, onlyColumns, columnList);
                    
                    
                    //Falls nicht alle Spalten im Record im XML angegeben, werden diese mit null befüllt
                    if (columnList != null && gPojo.getValueMap().size() > 0 &&  gPojo.getValueMap().size()<= columnList.size()) {
                        for (Column key : columnList) {
                            if (!gPojo.getValueMap().containsKey(key)) {
                                gPojo.addValue(key, new Data(null));
                            }else{
                                gPojo.addValue(key, new Data(key.getDataType().parse(gPojo.getValueMap().get(key).getValue().toString())));
                            }
                        }
                    }

                    if (gPojo.getValueMap().size() > 0) {
                        records.add(gPojo);
                    }
                    
                    if (onlyColumns) {
                        return;
                    }

                }
            }
        } catch (Exception e) {
            FacesContext fc = FacesContext.getCurrentInstance();
            if (fc != null) {
                fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, bundle.getString("IncorrectXMLFormat"), null));
            }
        }

    }

    //liest 1 Datensatz aus dem XML raus
    public GenericPojo getDataFromRecord(Node record, GenericPojo gPojo, boolean onlyColumns, List<Column> columnList) {
        Node children, child;
        String nodeName;
        String nodeValue;
        Column c;

        for (int k = 0; record != null && k < record.getChildNodes().getLength(); k++) {
            children = record.getChildNodes().item(k);
            if(onlyColumns && children.getChildNodes().getLength() == 0 )
                continue;
            for (int l = 0; children != null && l < children.getChildNodes().getLength(); l++) {
                child = children.getChildNodes().item(l);
                nodeName = child.getParentNode().getNodeName();
                nodeValue = child.getTextContent();
                
                if(columnList == null)
                    gPojo.addValue(new Column(nodeName), new Data(nodeValue));
                else{
                    c = columnList.get(l);
                    try {
                        gPojo.addValue(c, new Data(c.getDataType().parse(nodeValue)));
                    } catch (ParserException ex) {
                        Logger.getLogger(XMLParser.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        return gPojo;
    }

    @Override
    public void parseColumns() {
        try {
            parseDocument();

            if (elementsOfDoc != null) {

                readDataFromDoc(elementsOfDoc, true,null);      
                
                if (records != null && records.size() > 0) {
                    for (Object key : records.get(0).getValueMap().keySet()) 
                        columns.add((Column) key);
                                       
                    records.clear();
                }
            }

        } catch (ParserConfigurationException | IOException | org.xml.sax.SAXException ex) {
            Logger.getLogger(XMLParser.class
                    .getName()).log(Level.SEVERE, null, ex);
            FacesContext fc = FacesContext.getCurrentInstance();
            if (fc != null) {
                fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, ex.getMessage(), null));
            }
        }

    }

    @Override
    public void parseRecords(List<Column> columnList) {
        if (elementsOfDoc != null) {
            readDataFromDoc(elementsOfDoc, false,columnList);
        }
    }
    
}
*/