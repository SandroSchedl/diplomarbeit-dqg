package dataqualitygateway.service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.CustomizedRule;
import dataqualitygateway.dto.Data;
import dataqualitygateway.dto.DataCheck;
import dataqualitygateway.dto.GenericPojo;
import dataqualitygateway.dto.Result;
import dataqualitygateway.dto.Select;
import dataqualitygateway.dto.Template;
import dataqualitygateway.dto.enums.Rule;
import dataqualitygateway.infrastructure.dao.MongoDBDao;
import dataqualitygateway.infrastructure.dao.MySQLDao;
import dataqualitygateway.infrastructure.dao.NoSuchTemplateFoundException;
import dataqualitygateway.infrastructure.dao.connection.MongoDBConnectionManager;
import dataqualitygateway.infrastructure.parser.Parser;
import dataqualitygateway.infrastructure.parser.ParserFactory;
import java.util.ArrayList;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Collections;

import java.util.HashMap;

import java.util.HashSet;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.bson.Document;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author SandroSchedl
 */
@ManagedBean
@SessionScoped
public class Service implements Serializable {

    //Saves all templates which are found in the database
    private Set<Template> templates;

    private List<GenericPojo> recordList = new LinkedList<>();
    private List<Column> columnList = new LinkedList<>();

    //The object which is currently worked on
    private Result currentResult;
    private List<Result> lastResults = new LinkedList<>();
    private Parser parser;

    private MongoDBDao dao = new MongoDBDao();

    public void parseRecords() {
        if (parser == null || currentResult == null) {
            return;
        }

        setRecordIdAndRecordList(parser.getRecords(columnList));

    }

    private void setRecordIdAndRecordList(List<GenericPojo> records) {
        StringBuilder sb = new StringBuilder(currentResult.getResultId());
        String rec = "_rec";
        sb.append(rec);

        for (GenericPojo p : records) {
            sb.replace(sb.indexOf(rec) + rec.length(), sb.length(), Integer.toString(currentResult.increaseAndGetRecordCnt()));      //always replaces the old recordId with the next value
            p.setRecordId(sb.toString());
            recordList.add(p);
        }
    }

    public boolean database() throws InterruptedException {
        Result r = new Result(currentResult);
        Template t = r.getTemplate();

        System.out.println("In database");

        //if no template has been selected, the results are not written to the database
        if (!t.getName().equals("")) {
            t.setPreviousResults(dao.readResultsByTemplate(t));
//        new MySQLDao(currentResult, dao).writeRecords();
//        new MySQLDao(currentResult, dao).readRecordsByResult();
            dao.writeRecords(r);
            dao.updateTemplate(r.getTemplate(), false);
            t.addResult(r);
            dao.writeResult(r);
            return true;
        }

        return false;

    }
    
    //Method to get sorted previous results from db
    public List<Result> getLastResults() {
        try {
            if (lastResults == null) {
                lastResults = dao.readLastResults();
                Collections.sort(lastResults, (r1, r2) -> r2.getDate().compareTo(r1.getDate()));
            }
        } catch (NoSuchTemplateFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lastResults;
    }

    public void setRecordList(List<GenericPojo> recordList) {
        this.recordList = recordList;
    }

    /*
        The function is called in the FileUploadBean and initializes the currentResult
        and sets a reference to the recordList
     */
    public void initCurrentResult(String templateName, String fileName) {
        currentResult = new Result(getTemplateByName(templateName), fileName);
        recordList = currentResult.getRecordList();
    }

    public void addTemplate(String templateName, List<Column> columnListTest) {
        Template t = new Template(templateName, columnListTest);

        if (!templates.add(t)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Template already defined!", "Template: " + t.getName() + " could not be saved!"));
            return;
        }

        dao.writeTemplate(t);
    }

    public Template getTemplateByName(String templateName) {
        for (Template temp : templates) {
            if (temp.getName().equals(templateName)) {
                temp.setColumnList(dao.readColumns(temp));
                return temp;
            }
        }
        return new Template(templateName, columnList);
    }

    /*
        If the templates have not been initialized yet, they read the data from
        the database, else they return the templates
     */
    public Set<Template> getTemplates() {

        if (templates == null) {
            templates = dao.readTemplates(false);
        }
        return templates;

    }

    public String getRuleBooleanMap(GenericPojo p, Column c) {
        StringBuilder sb = new StringBuilder();
        Map<Rule, Boolean> map = p.getValueMap().get(c).getRuleMap();

        for (Rule r : map.keySet()) {
            sb.append(r.getName());
            sb.append(", Correct: ");
            sb.append(map.get(r) ? "YES" : "NO");
            sb.append("\n");
        }

        return sb.toString();
    }

    public void setStatistics(long time, int perf, int oneWrong, int wrong) {
        if (currentResult != null) {
            currentResult.setStatistics(time, perf, oneWrong, wrong);
        }
    }

    public List<Column> getColumnsFromFile(FileUploadEvent event) {
        parser = ParserFactory.getParser(event);
        columnList.addAll(parser.getColumns());
        return columnList;
    }
    
    public ResourceBundle getBundle(){
        FacesContext ctx = FacesContext.getCurrentInstance();
        Application app = ctx.getApplication();
        return app.getResourceBundle(ctx, "bundleVar");
    }

    public Map<Integer, Boolean> newChooserMap(List<Rule> ruleList) {
        Map<Integer, Boolean> cM = new HashMap<Integer, Boolean>();
        for (Rule r : ruleList) {
            cM.put(r.getId(), false);
        }
        return cM;
    }

    public void addData(GenericPojo pojo) {
        recordList.add(pojo);
    }

    public Result getCurrentResult() {
        return currentResult;
    }

    public void setCurrentResult(Result currentResult) {
        this.currentResult = currentResult;
    }

    public List<GenericPojo> getRecordList() {
        return recordList;
    }

    public List<Column> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<Column> columnList) {
        this.columnList = columnList;
    }

    public List<GenericPojo> getParsedRuleScreenList(FileUploadEvent event) {
        List<GenericPojo> gpList = new LinkedList<>();
        Parser p = ParserFactory.getParser(event);
        gpList = p.getRecords(p.getColumns());
        return gpList;
    }

}
