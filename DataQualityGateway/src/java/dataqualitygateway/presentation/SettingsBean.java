/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.presentation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;

/**
 *
 * @author Admin-PC
 */
@ManagedBean
@SessionScoped
public class SettingsBean {

    private String language = "English";
    private List<String> languages = new ArrayList<>();

    @ManagedProperty(value = "#{localeBean}")
    private LocaleBean locale;

    @ManagedProperty(value = "#{dashboardView}")
    private DashboardView dashboard;
    /**
     * Creates a new instance of SettingsBean
     */
    public SettingsBean() {
        languages.add("German");
        languages.add("English");
    }

    public LocaleBean getLocale() {
        return locale;
    }

    public void setLocale(LocaleBean locale) {
        this.locale = locale;
    }

    public DashboardView getDashboard() {
        return dashboard;
    }

    public void setDashboard(DashboardView dashboard) {
        this.dashboard = dashboard;
    }
    
    //Method to switch langugage
    public void switchLanguage(AjaxBehaviorEvent event) throws IOException {

        switch (language) {
            case "German":
                locale.setUserLoc("de");
                dashboard.init();
                break;
            case "English":
                locale.setUserLoc("en");
                dashboard.init();
                break;
        }
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

}
