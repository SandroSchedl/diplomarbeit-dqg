/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.presentation;

import java.util.Locale;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Admin-PC
 */
@ManagedBean
@SessionScoped
public class LocaleBean {
    private Locale userLoc;
    /**
     * Creates a new instance of LocaleBean
     */
    
    public LocaleBean() {
        userLoc = new Locale("en");
    }
    
    public String getUserLoc() { 
        return userLoc.getLanguage(); 
    } 
    
    public void setUserLoc(String language) { 
        userLoc = new Locale(language);
        FacesContext.getCurrentInstance().getViewRoot().setLocale(userLoc);
    }
    
}
