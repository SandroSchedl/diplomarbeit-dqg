/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.presentation;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.CustomizedRule;
import dataqualitygateway.dto.Data;
import dataqualitygateway.dto.GenericPojo;
import dataqualitygateway.dto.HasPDFValue;
import dataqualitygateway.dto.Result;
import dataqualitygateway.dto.enums.Rule;
import dataqualitygateway.service.Service;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

/**
 *
 * @author SandroSchedl
 */
@ManagedBean
@ViewScoped
public class DetailBean implements Serializable {

    private List<GenericPojo> recordList;
    private List<Column> columnList;
    private List<GenericPojo> filteredList;
    private List<Rule> ruleList;

    private Rule filteredRule;
    private Boolean resultCorrect;
    private Boolean ruleCorrect;

    private static final String STATUS_GREEN = "images/correctIcon.png";
    private static final String STATUS_RED = "images/incorrectIcon.png";

    @ManagedProperty(value = "#{service}")
    private Service service;

    @PostConstruct
    private void init() {
        recordList = service.getRecordList();
        columnList = service.getColumnList();
        filteredList = new LinkedList(recordList);
    }

    public boolean isRecordCorrect(GenericPojo p) {
        return p.isRecordCorrect();
    }

    public List<Column> getColumnListWithRules() {
        return columnList.stream().filter((col) -> !col.getRules().isEmpty()).collect(Collectors.toList());
    }

    //Liefert den Pfad für das Bild, welches im "Result" dargestellt wird
    public String getResultImage(GenericPojo p) {
        if (isRecordCorrect(p)) {
            return STATUS_GREEN;
        }

        return STATUS_RED;
    }

    public String getRuleBooleanMap(GenericPojo p, Column c) {
        if (recordList.contains(p)) {
            return service.getRuleBooleanMap(p, c);
        }

        return "";
    }

    /*
        Filter für die Prüfkriterien
        @param value ist das GenericPojo, bei dem die Regel überprüft wird
        @param filter ist der Boolean Wert, der den Status der Regel, nach welcher gefiltert wird, festlegt
     */
    public boolean ruleFilter(Object value, Object filter, Locale locale) {
        filter = ruleCorrect;
        String filterText = (filter == null) ? null : filter.toString().trim();
        if (filterText == null || filterText.isEmpty() || filteredRule == null) {
            return true;
        }
        if (value == null) {
            return false;
        }

        GenericPojo p = (GenericPojo) value;
        Boolean b;

        for (Data d : p.getValueMap().values()) {
            b = d.getRuleMap().get(filteredRule);
            if (b != null) {
                if (Objects.equals(b, filter)) {
                    return true;
                }
            }
        }

        return false;
    }

    //Setzt die Regel nach welcher gefiltert werden soll
    public void filteredRuleListener(ValueChangeEvent event) {
        filteredRule = (Rule) event.getNewValue();
//        if (filteredRule == null) //wenn "Select one" ausgewählt wird, dann wird der rule-filter auf "All" gesetzt
//        {
//            ruleCorrect = null;
//        }
    }

    //Filter für den gesamten Datensatz
    public void stateOfResultListener(ValueChangeEvent event) {
        resultCorrect = (Boolean) event.getNewValue();
    }

    //Setzt den Status der Regel nach welcher gefiltert wird
    public void stateOfRuleListener(ValueChangeEvent event) {
        ruleCorrect = (Boolean) event.getNewValue();
    }

    //Liefert die Liste aller Regeln, auf welche die Daten geprüft werden
    public List<Rule> getRuleList() {
        if (ruleList != null && !ruleList.isEmpty()) //wird dann zwar immer durchgeführt wenn auf keine Regeln geprüft wird aber wird gebraucht
        {
            return ruleList;
        }

        Set<Rule> ruleSet = new HashSet<>();

        for (GenericPojo p : recordList) {
            for (Data d : p.getValueMap().values()) {
                for (Rule r : d.getRuleMap().keySet()) {
                    ruleSet.add(r);
                }
            }
        }

        ruleList = new LinkedList(ruleSet);

        return ruleList;
    }

    public void downloadPDF() throws IOException, DocumentException {
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        Result r = service.getCurrentResult();

        StringBuilder sb = new StringBuilder("DQG_");
        sb.append(r.getFileName());
        sb.append("_");
        sb.append(r.getDate());
        sb.append(".pdf");

        String fileName = sb.toString();

        ec.responseReset();
        ec.setResponseContentType("application/pdf");
        ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        OutputStream output = ec.getResponseOutputStream();

        Document document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);
        PdfWriter writer = PdfWriter.getInstance(document, output);
        document.open();

        document.add(new Paragraph("File: " + r.getFileName() + ", Date: " + r.getDate()));
        document.add(new Paragraph("Results: "));
        document.addCreationDate();
        document.addTitle("Results");
        document.addSubject("Results of the check on the file '" + r.getFileName() + "' on " + r.getDate());

        PdfPTable mainTable = new PdfPTable(1);
        mainTable.setHeaderRows(1);
        mainTable.setWidthPercentage(100);
        mainTable.setSpacingBefore(10f);
        mainTable.setSpacingAfter(10f);

        Font headerFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 8);
        Font recordRuleFont = FontFactory.getFont(FontFactory.HELVETICA, 6);

        //inserts the column headers
        insertDataToPDFTable(columnList, mainTable, headerFont);

        int amountOfRules = getRuleList().size();
        PdfPCell emptyCell = new PdfPCell();
        emptyCell.setMinimumHeight(5);
        emptyCell.setBorderWidth(0);

        boolean isRecordCorrect;

        for (GenericPojo p : filteredList) {
            //inserts the record values
            isRecordCorrect = insertDataToPDFTable(p.getValueMap().values(), mainTable, recordRuleFont);

            //if rules are selected, they and the results are added to the table
            if (amountOfRules > 0) {
                mainTable.addCell(getRuleDataAsTable(p, recordRuleFont, isRecordCorrect));
            }

            mainTable.addCell(emptyCell);
        }

        document.add(mainTable);

        document.close();
        writer.close();

        fc.responseComplete();
    }

    private boolean insertDataToPDFTable(Collection<? extends HasPDFValue> coll, PdfPTable table, Font font) {
        PdfPCell cell = new PdfPCell();
        boolean retVal = true;
        Paragraph p = new Paragraph();

        Font wrongRecordFont = new Font(font);
        wrongRecordFont.setColor(BaseColor.RED);
        wrongRecordFont.setStyle("bold");

        Chunk c;
        Chunk separator = new Chunk(" // ", font);

        for (HasPDFValue val : coll) {
            c = new Chunk();

            if (val.isValueCorrect()) {
                c.setFont(font);
            } else {
                c.setFont(wrongRecordFont);
                retVal = false;
            }

            c.append(val.getPDFValue());
            p.add(c);
            p.add(separator);
        }

        cell.addElement(p);
        cell.setPadding(5f);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);

        table.addCell(cell);
        
        return retVal;
    }

    private PdfPTable getRuleDataAsTable(GenericPojo p, Font font, boolean isRecordCorrect) {
        PdfPTable ruleTable = new PdfPTable(1);
        ruleTable.setWidthPercentage(100);
        PdfPCell cell;
        StringBuilder sb;

        Font ruleHeaderFont = new Font(font);
        ruleHeaderFont.setStyle("bold");

        if (isRecordCorrect) {
            ruleHeaderFont.setColor(0,128,0);
        } else {
            ruleHeaderFont.setColor(BaseColor.RED);
        }

        Paragraph ruleHeader = new Paragraph("Rules: ",ruleHeaderFont);

        ruleTable.addCell(ruleHeader);

        for (Entry<Column, Data> entry : p.getValueMap().entrySet()) {
            for (Map.Entry<Rule, Boolean> e : entry.getValue().getRuleMap().entrySet()) {
                cell = new PdfPCell();
                cell.setPadding(2f);
                sb = new StringBuilder(e.getKey().getName());

                sb.append(" : ");
                sb.append(e.getValue());

                cell.addElement(new Paragraph(sb.toString(), font));
                ruleTable.addCell(cell);
            }
        }

        return ruleTable;
    }
    
    public List<GenericPojo> getFilteredList() {
        return filteredList;
    }

    public void setFilteredList(List<GenericPojo> filteredList) {
        this.filteredList = filteredList;

    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public List<GenericPojo> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<GenericPojo> recordList) {
        this.recordList = recordList;
    }

    public List<Column> getColumnList() {
        return columnList;
    }

    public Rule getFilteredRule() {
        return filteredRule;
    }

    public void setFilteredRule(Rule filteredRule) {
        this.filteredRule = filteredRule;
    }

    public Boolean getResultCorrect() {
        return resultCorrect;
    }

    public void setResultCorrect(Boolean resultCorrect) {
        this.resultCorrect = resultCorrect;
    }

    public Boolean getRuleCorrect() {
        return ruleCorrect;
    }

    public void setRuleCorrect(Boolean ruleCorrect) {
        this.ruleCorrect = ruleCorrect;
    }

}
