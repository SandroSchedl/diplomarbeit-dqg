/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.presentation;

import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.QuoteOfTheDay;
import dataqualitygateway.dto.Result;
import dataqualitygateway.service.Service;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.concurrent.ThreadLocalRandom;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.BubbleChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.DonutChartModel;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

/**
 *
 * @author Admin-PC
 */

@ManagedBean
@SessionScoped
public class DashboardView{

    private DashboardModel model;
    private Result result = null;
    
    private BarChartModel barChart;
    private DonutChartModel donutModel;
    private LineChartModel lineModel;
    private BubbleChartModel bubbleModel;
    
    private int anzIncorrectRecords;
    private String duration;

    @ManagedProperty(value = "#{service}")
    private Service service;

    public Result getResult() {
        return result;
    }
    
    public String getQuoteOfTheDay(){
        int randomNum = ThreadLocalRandom.current().nextInt(0, QuoteOfTheDay.QUOTES.length - 1);
        return QuoteOfTheDay.QUOTES[randomNum];
    }


    public void setResult(Result result) {
        this.result = result;
        this.anzIncorrectRecords = result.getAnzRecordsWithMoreErrors() + result.getAnzRecordsWithOneError();
        BigDecimal seconds = new BigDecimal(result.getTime());

        int[] time = splitToComponentTimes(seconds);

        duration = "" + time[0] + "h " + time[1] + "min " + time[2] + "s";
    }

    public static int[] splitToComponentTimes(BigDecimal biggy) {
        long longVal = biggy.longValue();
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        int[] ints = {hours, mins, secs};
        return ints;
    }

    private BarChartModel initBarModel() {
        ResourceBundle bundle = service.getBundle();
        
        BarChartModel barModel = new BarChartModel();
        barModel.setSeriesColors("74B23D, FFC400, cc0000");

        ChartSeries correct = new ChartSeries();
        correct.setLabel(bundle.getString("legendeCorrect"));
        correct.set(" ", result.getAnzCorrectRecords());

        ChartSeries oneError = new ChartSeries();
        oneError.setLabel(bundle.getString("legendeMiddlecorrect"));
        oneError.set(" ", result.getAnzRecordsWithOneError());

        ChartSeries moreErrors = new ChartSeries();
        moreErrors.setLabel(bundle.getString("legendeIncorrect"));
        moreErrors.set(" ", result.getAnzRecordsWithMoreErrors());

        barModel.addSeries(correct);
        barModel.addSeries(oneError);
        barModel.addSeries(moreErrors);

        return barModel;
    }

    public BubbleChartModel getBubbleModel() {
        return bubbleModel;
    }

    public void setBubbleModel(BubbleChartModel bubbleModel) {
        this.bubbleModel = bubbleModel;
    }

    private DonutChartModel initDonutModel() {
        ResourceBundle bundle = service.getBundle();

        DonutChartModel donutModel = new DonutChartModel();

        Map<String, Number> circle = new LinkedHashMap<>();
        circle.put(bundle.getString("legendeCorrect"), result.getRecordList().isEmpty() ? 1 : result.getAnzCorrectRecords() * 100 / result.getRecordList().size());
        circle.put(bundle.getString("legendeMiddlecorrect"), result.getRecordList().isEmpty() ? 1 : result.getAnzRecordsWithOneError() * 100 / result.getRecordList().size());
        circle.put(bundle.getString("legendeIncorrect"), result.getRecordList().isEmpty() ? 1 : result.getAnzRecordsWithMoreErrors() * 100 / result.getRecordList().size());
        donutModel.addCircle(circle);

        return donutModel;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    private void createDonutModels() {
        donutModel = initDonutModel();
        donutModel.setExtender("chartExtender");
        donutModel.setSliceMargin(5);
        donutModel.setSeriesColors("74B23D, FFC400, cc0000");
        donutModel.setDatatipFormat("%2$.2d%%");
        donutModel.setLegendPosition("e");
        donutModel.setDataLabelFormatString("%d%%");
        donutModel.setShowDataLabels(true);
        donutModel.setDataFormat("value");
        donutModel.setShadow(false);
    }

    private void createLineModel() {
        ResourceBundle bundle = service.getBundle();

        lineModel = initLinearModel();
        lineModel.setTitle(bundle.getString("capabilityOfErrors"));
        lineModel.setSeriesColors("f90");
        //lineModel.setShowPointLabels(false);
        //lineModel.setStacked(true);
        lineModel.setExtender("lineExtender");

        Axis xAxis = lineModel.getAxis(AxisType.X);
        xAxis.setLabel(bundle.getString("records"));
        xAxis.setMax(result.getRecordList().size() - 1);
        xAxis.setMin(0);
        xAxis.setTickFormat("%d");
        
        Axis yAxis = lineModel.getAxis(AxisType.Y);
        yAxis.setLabel(bundle.getString("numberOfErrors"));
        yAxis.setTickFormat("%.2f");

        int max = 0;

        for (int i = 0; i < result.getRecordList().size(); i++) {
            if (max < result.getRecordList().get(i).getErrorCounter()) {
                max = result.getRecordList().get(i).getErrorCounter();
            }
        }
        
        int min = max;
        
        for (int i = 0; i < result.getRecordList().size(); i++) {
            if (min > result.getRecordList().get(i).getErrorCounter()) {
                min = result.getRecordList().get(i).getErrorCounter();
            }
        }

        yAxis.setMax(max + 0.5);
        yAxis.setMin(min - 0.5);
    }

    private LineChartModel initLinearModel() {
        LineChartModel model = new LineChartModel();

        LineChartSeries series1 = new LineChartSeries();
        series1.setShowLine(false);

        Column col = null;
        
        //searching for column "id" in first record of recordlist
        if(result.getRecordList().size() > 0){
        for (Column c : result.getRecordList().get(0).getValueMap().keySet()) {
            if (Objects.equals(c.getName().toLowerCase(), "id")) {
                col = c;
            }
        }
        }
        
        //sets values in graph -> errors per record
        //using id or sequential number if no id available
        for (int i = 0; i < result.getRecordList().size(); i++) 
             series1.set(col != null ? Integer.parseInt(String.valueOf(result.getRecordList().get(i).getValueMap().get(col).getValue())) : i, result.getRecordList().get(i).getErrorCounter());
        

        model.addSeries(series1);

        return model;
    }

//    private void setSeries(Result result, int i, Column col, LineChartSeries series1, LineChartSeries series2) {
//        switch (result.getRecordList().get(i).getErrorCounter()) {
//            case 0:
//                series1.set(col != null ? Integer.parseInt((String)result.getRecordList().get(i).getValueMap().get(col).getValue()) : i, result.getRecordList().get(i).getErrorCounter());
//                break;
//            case 1:
//                series2.set(col != null ? Integer.parseInt((String)result.getRecordList().get(i).getValueMap().get(col).getValue()) : i, result.getRecordList().get(i).getErrorCounter());
//                break;
//        }
//    }

    private void createAnimatedModels() {
        ResourceBundle bundle = service.getBundle();
        // barChart.setExtender("chartExtender");
        barChart = initBarModel();
        barChart.setAnimate(true);
        barChart.setExtender("skinBar");
        barChart.setDatatipFormat("%2$.2d");
        barChart.setLegendPosition("ne");
        
        Axis yAxis = barChart.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(result.getRecordList().size());
        yAxis.setLabel(bundle.getString("records"));
        yAxis.setTickFormat("%.1f");

    }

    public BarChartModel getBarChart() {
        return barChart;
    }

    public void setBarChart(BarChartModel barChart) {
        this.barChart = barChart;
    }

    public DonutChartModel getDonutModel() {
        return donutModel;
    }

    public void setDonutModel(DonutChartModel donutModel) {
        this.donutModel = donutModel;
    }

    public int getAnzIncorrectRecords() {
        return anzIncorrectRecords;
    }

    public void setAnzIncorrectRecords(int anzIncorrectRecords) {
        this.anzIncorrectRecords = anzIncorrectRecords;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void init() {
        if (result != null) {
            createAnimatedModels();
            createDonutModels();
            createLineModel();

            model = new DefaultDashboardModel();
            DashboardColumn column1 = new DefaultDashboardColumn();
            DashboardColumn column2 = new DefaultDashboardColumn();
            DashboardColumn column3 = new DefaultDashboardColumn();
            DashboardColumn column4 = new DefaultDashboardColumn();
            
            column1.addWidget("records");
            column1.addWidget("donutChart");
            column1.addWidget("graph");
                   
            column2.addWidget("correct");
            
            column3.addWidget("incorrect");
            column3.addWidget("barChart");     
                   
            column4.addWidget("duration");

            model.addColumn(column1);
            model.addColumn(column2);
            model.addColumn(column3);
            model.addColumn(column4);
        }
    }

//    public void handleReorder(DashboardReorderEvent event) {
//        FacesMessage message = new FacesMessage();
//        message.setSeverity(FacesMessage.SEVERITY_INFO);
//        message.setSummary("Reordered: " + event.getWidgetId());
//        message.setDetail("Item index: " + event.getItemIndex() + ", Column index: " + event.getColumnIndex() + ", Sender index: " + event.getSenderColumnIndex());
//
//        addMessage(message);
//    }
//
//    public void handleClose(CloseEvent event) {
//        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Panel Closed", "Closed panel id:'" + event.getComponent().getId() + "'");
//
//        addMessage(message);
//    }

    public LineChartModel getLineModel() {
        return lineModel;
    }

    public void setLineModel(LineChartModel lineModel) {
        this.lineModel = lineModel;
    }

//    public void handleToggle(ToggleEvent event) {
//        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, event.getComponent().getId() + " toggled", "Status:" + event.getVisibility().name());
//
//        addMessage(message);
//    }

//    private void addMessage(FacesMessage message) {
//        FacesContext.getCurrentInstance().addMessage(null, message);
//    }

    public DashboardModel getModel() {
        return model;
    }
}
