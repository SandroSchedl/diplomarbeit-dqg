/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.presentation;

import dataqualitygateway.dto.enums.DataType;
import dataqualitygateway.dto.GenericPojo;
import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.CustomizedRule;
import dataqualitygateway.dto.Data;

import dataqualitygateway.dto.Group;
import dataqualitygateway.dto.Result;
import dataqualitygateway.dto.RuleHelper;
import dataqualitygateway.dto.Template;

import dataqualitygateway.dto.Select;
import dataqualitygateway.dto.SelectGroup;

import dataqualitygateway.dto.enums.Rule;
import dataqualitygateway.infrastructure.dao.NoSuchTemplateFoundException;
import dataqualitygateway.service.Service;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author SandroSchedl
 */
@ManagedBean
@SessionScoped
public class FileUploadBean {

    // Map to save the choosen Rules 
    //   private Map<Integer, Boolean> chooserMap = new HashMap<Integer, Boolean>();
    private List<DataType> typeList = DataType.getDataTypes();
    private List<Rule> beanRuleList = Rule.getRules();
    private List<Group> groupsList = fillGroupList();
    private List<Select> selectListForGroups = this.newChooserList(beanRuleList);
    private List<SelectGroup> selectGroup = new LinkedList<>();
    private String fileName;
    private String newTemplateName;
    
    private boolean databaseInProcess = false;
    private boolean initialPoll = true;
    private Semaphore sem = new Semaphore(1,true);

    private List<Column> columnList = new LinkedList<>();

    public static ResourceBundle bundle;

    private List<GenericPojo> compareList = new LinkedList<>();

    //Variable which initalizes the choosen Rule
    private Column c;
    private Group group;
    private String templateName;

    @ManagedProperty(value = "#{service}")
    private Service service;

    @ManagedProperty(value = "#{dashboardView}")
    private DashboardView dashboard;

    //initalize right menu panel
    @PostConstruct
    public void initRightPanel() {
        service.getLastResults();
    }

    //returns if the polling should stop
    public boolean displayMessageOnStop() {
        if(initialPoll){
            return true;
        }
        
        
        if(databaseInProcess)
            return false;
        else
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database", "Successfully uploaded data to database!"));
        
        return true;
    }

    public boolean isDatabaseInProcess() {
        return databaseInProcess;
    }

    public void setDatabaseInProcess(boolean databaseInProcess) {
        this.databaseInProcess = databaseInProcess;
    }

//function to check if the type of the data equals the type you have choosen 
//    public void typeChooser(Column col) {
//
//        for (GenericPojo gp : service.getRecordList()) {
//
//            for (Map.Entry<Column, Data> data : gp.getValueMap().entrySet()) {
//                if (data.getKey().getName().equals(col.getName())) {
//                    if (!col.getDataType().equals(DataType.STRING)) {
//                        data.getValue().setTypeOfValue(col.getDataType());
//                        data.getValue().getRuleMap().put(Rule.TYPECHECKER, Rule.TYPECHECKER.check(data.getValue()));
//                        System.out.println(Rule.TYPECHECKER + "    " + Rule.TYPECHECKER.check(data.getValue()) + "   " + data.getValue().getTypeOfValue());
//                    } else {
//                        data.getValue().getRuleMap().put(Rule.TYPECHECKER, Rule.TYPECHECKER.check(data.getValue()));
//                        //  entry2.getValue().getTypeCheckMap().put(entry2.getValue().getRuleMap(), entry2.getValue().getTypeOfValue().getName());
//                        System.out.println(Rule.TYPECHECKER + "    " + Rule.TYPECHECKER.check(data.getValue()) + "   " + data.getValue().getTypeOfValue());
//                    }
//                }
//            }
//
//        }
//
//    }

    //  UIComponent uic = ev.getComponent();
    //Function that starts the Examination
    private void start() {
        long start = System.nanoTime();
        boolean exam;
     

//        customRuleList = service.getCustomRules();
        for (Column col : columnList) {
            System.out.println(col.getName());
            //        typeChooser(col);
            for (SelectGroup sg : col.getGroupList()) {
                if (sg.isChooser()) {
                    for (GenericPojo gp : service.getRecordList()) {

                        for (Map.Entry<Column, Data> data : gp.getValueMap().entrySet()) {

                            if (data.getKey().equals(col)) {
                                for (Select gs : sg.getG().getGroupList()) {
                                    exam = gs.getR().check(data.getValue(), gs.getCr());
                                    data.getValue().addRule(gs.getR(), exam);
                                    System.out.println(gs.getR() + "    " + exam);

                                    if (!exam) {
                                        gp.setErrorCounter(gp.getErrorCounter() + 1);
                                    }

                                }
                            }

                        }
                    }
                }

            }
            for (Select s : col.getChooserList()) {

                if (s.isChooser()) {
                    for (GenericPojo gp : service.getRecordList()) {

                        for (Map.Entry<Column, Data> data : gp.getValueMap().entrySet()) {

                            if (data.getKey().equals(col)) {
                                if (s.getR().equals(Rule.TABLECHECKER)) {
                                    exam = s.getR().check(data.getValue(), col.getCompareList());
                                } else {
                                    exam = s.getR().check(data.getValue(), col.getCustomRule());
                                }
                                data.getValue().addRule(s.getR(), exam);
                                System.out.println(s.getR() + "    " + exam + "    " + data.getValue().getValue());

                                if (!exam) {
                                    data.getValue().increaseErrorCounter();
                                    gp.setErrorCounter(gp.getErrorCounter() + 1);
                                }

                            }

                        }
                    }
                }

            }
        }

        //Zeit wie lange die Prüfung gedauert hat
        long time = System.nanoTime() - start;
        //Auswertung der Prüfung
        int perfect = 0, oneWrong = 0, wrong = 0;
        for (GenericPojo gp : service.getRecordList()) {

            if (gp.getErrorCounter() == 0) {
                perfect++;
            }
            if (gp.getErrorCounter() == 1) {
                oneWrong++;
            }
            if (gp.getErrorCounter() > 1) {
                wrong++;

            }

        }

        time = TimeUnit.SECONDS.convert(time, TimeUnit.NANOSECONDS);

        setStatistics(time, perfect, oneWrong, wrong);

    }

    //Function that initalizes the Column variable
    public void saveColumn(Column column) {
        this.c = column;
    }

    //Function that initalizes the Group variable
    public void saveGroup(Group g) {
        this.group = g;
    }

    //Function to save the choosen rules
    public void saveRules() {

        for (Column col : columnList) {

            if (col.getName().equals(c.getName())) {
                col = c;
                System.out.println(c.getGroupList());
            }
        }

    }

    public void saveGroupsFromDialog() {

        for (Column col : columnList) {

            if (col.getName().equals(c.getName())) {
                col = c;
            }
        }

    }

    //Function to create Groups out of rules
    public void saveGroupRules() {
        CustomizedRule newCustomGroup = new CustomizedRule(valueRangeLeft, valueRangeRight, country, bannedWords, dateFormat, parameter);
        List<Select> help = new ArrayList<>();
        for (Select s : selectListForGroups) {
            if (s.isChooser()) {
                s.setCr(newCustomGroup);
                help.add(s);
            }

        }
        groupsList.add(new Group(groupID, groupName, help));
        this.selectGroup.add(new SelectGroup(false, new Group(groupID, groupName, help)));
        for (Column colom : this.columnList) {
            colom.addSelectGroup(new SelectGroup(false, new Group(groupID, groupName, help)));
        }
    }

//    public boolean advancedChooser(Rule r) {
//        for (Rule r1 : beanRuleList) {
//            for (Map.Entry<Integer, Boolean> entry : c.getChooserMap().entrySet()) {
//                if (r1.getId() == entry.getKey()) {
//                    return entry.getValue();
//                }
//            }
//        }
//        return false;
//    }
    public Object toRule() {
        return "toRule";
    }

    public Object toGen() {
        return "toGen";
    }

    //is called after the "Upload" button is pressed and sets the columns in the datatable
    public void handleFileUpload(FileUploadEvent event) {

        setColumnsInDataTable(event);
        fileName = event.getFile().getFileName();
    }

    //is called after the Upload Button in the RuleScreen getsPressed 
    public void handleTableFileUpload(FileUploadEvent event) {
        c.setCompareList( service.getParsedRuleScreenList(event));
    }
    //parses the records and starts checking them

    public String startChecking() throws InterruptedException, NoSuchTemplateFoundException {
        service.initCurrentResult(templateName, fileName);
        //do muas ma dann no schaun wenn da templatenoman a laastring is (also template selected) wia ma do dua

        service.parseRecords();

        start();

        service.getLastResults().add(service.getCurrentResult());
        dashboard.setResult(service.getCurrentResult());
        Collections.sort(service.getLastResults(), (r1, r2) -> r2.getDate().compareTo(r1.getDate()));
        dashboard.init();
        
        if(sem.availablePermits() < 1)
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Database", "Waiting for active upload to finish!"));

//        new Thread(() -> {
//            try {
//                    
//                sem.acquire();
                    
                if (!templateName.equals("")) {
                    initialPoll = false;
                    
                    databaseInProcess = true;
                    service.database();
                }
                
//            } catch (InterruptedException ex) {
//                Logger.getLogger(FileUploadBean.class.getName()).log(Level.SEVERE, null, ex);
//            }finally{
//                sem.release();
//                databaseInProcess = false;
//            }
//        }, "Database-Thread").start();

        return "toDashboard";
    }

    //history function -> changes currentResult to required
    public String changeResult(Result check) {
        service.setCurrentResult(check);
        service.setColumnList(check.getTemplate().getColumnList());
        service.setRecordList(check.getRecordList());

        //initialize dashboard with new result
        dashboard.setResult(check);
        dashboard.init();

        //initialize dashboard with new result
        dashboard.setResult(check);
        dashboard.init();

        //navigate to dashboard.xhtml if index.xhtml
        return "toDashboard";
    }

    public DashboardView getDashboard() {
        return dashboard;
    }

    public void setDashboard(DashboardView dashboard) {
        this.dashboard = dashboard;
    }

    //Sets the columns in the datatable
    private void setColumnsInDataTable(FileUploadEvent event) {
        columnList.clear();
        columnList = service.getColumnsFromFile(event);

    }

    public void setStatistics(long time, int perf, int oneWrong, int wrong) {
        service.setStatistics(time, perf, oneWrong, wrong);
    }

    public void templateSelected(ValueChangeEvent event) {
        Object o = event.getNewValue();
        templateName = String.valueOf(o);

        List<Column> colList = service.getTemplateByName(templateName).getColumnList();
        if (colList != null && !colList.isEmpty()) {
            if (columnList.equals(colList) || columnList.isEmpty()) {
                columnList = colList;
            } else {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "Cannot apply template to current dataset", "Template: " + templateName + " has a different structure than the dataset!"));
            }
        }

    }

    public List<Select> getChooserListBySelectedColumn(Column c) {
        if (c == null) {
            return null;
        }

        return c.getChooserList();
    }

    public List<SelectGroup> getGroupChooserListBySelectedColumn(Column colo) {
        if (colo == null) {
            return null;
        }
//
//        colo.setGroupList(compareLists(colo.getGroupList()));

        return colo.getGroupList();
    }

    public List<String> getTemplates() {
        List<String> retVal = new LinkedList();

        for (Template t : service.getTemplates()) {
            retVal.add(t.getName());
        }

        return retVal;
    }

    public void createTemplate() {
        service.addTemplate(newTemplateName, columnList);
    }

    public Set<Template> getTemplateList() {
        return service.getTemplates();
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public List<DataType> getTypeList() {
        return DataType.getDataTypes();
    }

    public List<Column> getColumnList() {
        return columnList;
    }

//    public Map<Integer, Boolean> getChooserMap() {
//        return chooserMap;
//    }
//
//    public void setChooserMap(Map<Integer, Boolean> chooserMap) {
//        this.chooserMap = chooserMap;
//    }
//
    public List<Rule> getBeanRuleList() {
        return beanRuleList;
    }
//
//    public boolean isChooser() {
//        return chooser;
//    }
//
//    public void setChooser(boolean chooser) {
//        this.chooser = chooser;
//    }

    public Column getC() {
        return c;
    }

    public void setC(Column c) {
        this.c = c;
    }

//    public boolean isCustomize(Rule r) {
//        boolean retVal = false;
//
//        if (r.getCustomizable() == 1) {
//            retVal = true;
//        }
//
//        return retVal;
//    }
    public String saveCustomizedRules() {
        if (!(country == null)) {
            c.setCustomRule(new CustomizedRule(valueRangeLeft, valueRangeRight, country, bannedWords, dateFormat, parameter));
            saveRules();

        }
        return "toGen";
    }
    //Variablen für die ValueRange Prüfung
    private int valueRangeLeft, valueRangeRight;

    //Liste für alle Länder auf English
    private List<String> countryList = initalizeCountries();

    //Liste für alle Datumsformate
    private List<String> dateList = initalizeDateList();

    //Variablen die Informationen für die Prüfregeln speichern
    private String country;
    private String bannedWords;
    private String dateFormat;
    private String parameter;

    //Variablen zum Gruppen erstellen
    private int groupID;
    private String groupName;

    public int getValueRangeLeft() {
        return valueRangeLeft;
    }

    public void setValueRangeLeft(int valueRangeLeft) {
        this.valueRangeLeft = valueRangeLeft;
    }

    public int getValueRangeRight() {
        return valueRangeRight;
    }

    public void setValueRangeRight(int valueRangeRight) {
        this.valueRangeRight = valueRangeRight;
    }

    private List<String> initalizeCountries() {
        RuleHelper c = new RuleHelper();
        List<String> list = new ArrayList<>();
        for (String countryCode : c.getCountries()) {

            Locale obj = new Locale("en", countryCode);
            list.add(obj.getDisplayCountry(obj));
        }
        return list;
    }

    public List<String> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<String> countryList) {
        this.countryList = countryList;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBannedWords() {
        return bannedWords;
    }

    public void setBannedWords(String bannedWords) {
        this.bannedWords = bannedWords;
    }

    private List<String> initalizeDateList() {
        List<String> list = new ArrayList<>();
        list.add("yyyy-MM-dd");
        list.add("dd-MM-yyyy");
        list.add("dd-MM-yy");

        return list;
    }

    public List<String> getDateList() {
        return dateList;
    }

    public void setDateList(List<String> dateList) {
        this.dateList = dateList;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    private List<Group> fillGroupList() {
        List<Group> groupUnFilledList = new LinkedList<>();
        List<Select> selectListForGroup = new LinkedList<>();
        selectListForGroup.add(new Select(Rule.EXISTINGCOUNTRY, true));
        selectListForGroup.add(new Select(Rule.ISEUROPE, true));
        selectListForGroup.add(new Select(Rule.ISCOUNTRY, true, new CustomizedRule("Austria")));
        groupUnFilledList.add(new Group(1, "CountryGroup", selectListForGroup));
        return groupUnFilledList;
    }

    public List<Group> getGroupsList() {
        return groupsList;
    }

    public void setGroupsList(List<Group> groupList) {
        this.groupsList = groupList;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<Select> newChooserList(List<Rule> ruleList) {
        List<Select> selList = new ArrayList<>();
        for (Rule r : ruleList) {
            selList.add(new Select(r, false));
        }
        return selList;
    }

    public List<Select> getSelectListForGroups() {
        return selectListForGroups;
    }

    public void setSelectListForGroups(List<Select> selectListForGroups) {
        this.selectListForGroups = selectListForGroups;
    }

    public int getGroupID() {
        return groupID;
    }

    public void setGroupID(int groupID) {
        this.groupID = groupID;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<SelectGroup> compareLists(List<SelectGroup> gList) {
        List<SelectGroup> retVal = new LinkedList<>();
        if (gList == null) {
            for (Group gro : groupsList) {

                retVal.add(new SelectGroup(false, gro));
            }
            return retVal;
        }

        if (!(gList.size() == this.groupsList.size())) {
            for (SelectGroup sgG : selectGroup) {
                if (!(gList.contains(sgG.getG()))) {
                    gList.add(sgG);
                }
            }
            return gList;
        } else {
            return gList;
        }
    }

    public String getNewTemplateName() {
        return newTemplateName;
    }

    public void setNewTemplateName(String newTemplateName) {
        this.newTemplateName = newTemplateName;
    }

    public String toTemplate() {
        return "toTemplate";
    }
    public List<Column> setTemplateColumnList(String name){
        return service.getTemplateByName(name).getColumnList();
    }
}
