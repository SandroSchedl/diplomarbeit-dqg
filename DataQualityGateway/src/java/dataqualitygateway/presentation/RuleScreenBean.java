/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.presentation;

import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.CustomizedRule;
import dataqualitygateway.dto.RuleHelper;
import dataqualitygateway.service.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author max
 */
@ManagedBean
@SessionScoped
public class RuleScreenBean {

    @ManagedProperty(value = "#{service}")
    private Service service;

    //Variablen für die ValueRange Prüfung
    private int valueRangeLeft, valueRangeRight;

    //Liste für alle Länder auf English
    private List<String> countryList = initalizeCountries();

    //Liste für alle Datumsformate
    private List<String> dateList = initalizeDateList();

    //Variablen die Informationen für die Prüfregeln speichern
    private String country;
    private String bannedWords;
    private String dateFormat;
    private String parameter;

    public int getValueRangeLeft() {
        return valueRangeLeft;
    }

    public void setValueRangeLeft(int valueRangeLeft) {
        this.valueRangeLeft = valueRangeLeft;
    }

    public int getValueRangeRight() {
        return valueRangeRight;
    }

    public void setValueRangeRight(int valueRangeRight) {
        this.valueRangeRight = valueRangeRight;
    }

    private List<String> initalizeCountries() {
        RuleHelper c = new RuleHelper();
        List<String> list = new ArrayList<>();
        for (String countryCode : c.getCountries()) {

            Locale obj = new Locale("en", countryCode);
            list.add(obj.getDisplayCountry(obj));
        }
        return list;
    }

    public List<String> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<String> countryList) {
        this.countryList = countryList;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBannedWords() {
        return bannedWords;
    }

    public void setBannedWords(String bannedWords) {
        this.bannedWords = bannedWords;
    }

    private List<String> initalizeDateList() {
        List<String> list = new ArrayList<>();
        list.add("yyyy-MM-dd");
        list.add("dd-MM-yyyy");
        list.add("dd-MM-yy");

        return list;
    }

    public List<String> getDateList() {
        return dateList;
    }

    public void setDateList(List<String> dateList) {
        this.dateList = dateList;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

//    public void saveCustomizedRules(Column c) {
//        if (!(country == null)) {
//            service.fillCusomList(new CustomizedRule(valueRangeLeft, valueRangeRight, country, bannedWords, dateFormat, parameter, c));
//        }
//    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

}
