/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.infrastructure.parser;

import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.GenericPojo;
import dataqualitygateway.dto.enums.DataType;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Admin-PC
 */

//for testing FacesMessages lines in catches in Parsers must be commented out
//otherwise FacesContext would occur in a NullPointerException
public class ParserTest {

    private Parser jsonParser, csvParser, xmlParser;
    private List<Column> columnList;
    private Column c1, c2, c3, c4;

    @Before
    public void setUp() throws UnsupportedEncodingException {
        String jsonStr = "", csvStr = "", xmlStr = "";
        InputStream s = null;
        
        columnList = new ArrayList<>();

        c1 = new Column("id");
        c1.setDataType(DataType.STRING);
        columnList.add(c1);
        
        c2 = new Column("title");
        c2.setDataType(DataType.STRING);
        columnList.add(c2);
        
        c3 = new Column("country");
        c3.setDataType(DataType.STRING);
        columnList.add(c3);
        
        c4 = new Column("city");
        c4.setDataType(DataType.STRING);
        columnList.add(c4);
        
        jsonStr = "[{\"title\":\"Prosecco Villa Sandi DOC\",\"country\":\"Italy\",\"city\":\"Napoli\"}," +
                        "{\"title\":\"Hugo Deluxe, inclusive\",\"city\":\"Vienna\"}]";
       
        csvStr = ",title,country,city\n"
                + "0,Prosecco Villa Sandi DOC,Italy,Napoli\n"
                + ",\"Hugo Deluxe, inclusive\",,Vienna\n" +
                ",\"Hugo Deluxe, inclusive\",Vienna";

        xmlStr = createXmlString();


        s = new ByteArrayInputStream(jsonStr.getBytes("UTF-8"));
        jsonParser = new JSONParserr(s);
        s = new ByteArrayInputStream(csvStr.getBytes("UTF-8"));
        csvParser = new CSVParser(s);
        s = new ByteArrayInputStream(xmlStr.getBytes("UTF-8"));
        xmlParser = new XMLParser(s);
        
    }
    
    @Test
    public void testCSVParser() {
        List<Column> columns = csvParser.getColumns();
        
        Column c = new Column("id");
        assertEquals(true, columns.contains(c));
        c.setName("title");
        assertEquals(true, columns.contains(c));
        c.setName("country");
        assertEquals(true, columns.contains(c));
        c.setName("city");
        assertEquals(true, columns.contains(c));     
        
        List<GenericPojo> records = csvParser.getRecords(columnList);
        assertEquals(records.get(0).getValueMap().get(columnList.get(0)).getValue(), "0");
        assertEquals(records.get(0).getValueMap().get(columnList.get(1)).getValue(), "Prosecco Villa Sandi DOC");
        assertEquals(records.get(0).getValueMap().get(columnList.get(2)).getValue(), "Italy");
        assertEquals(records.get(0).getValueMap().get(columnList.get(3)).getValue(), "Napoli");
        
        assertEquals(records.get(1).getValueMap().get(columnList.get(0)).getValue(), null);
        assertEquals(records.get(1).getValueMap().get(columnList.get(1)).getValue(), "Hugo Deluxe, inclusive");
        assertEquals(records.get(1).getValueMap().get(columnList.get(2)).getValue(), null);
        assertEquals(records.get(1).getValueMap().get(columnList.get(3)).getValue(), "Vienna");

        assertEquals(records.size(), 2);
    }

    @Test
    public void testJSONParser() {
        List<Column> columnArray = jsonParser.getColumns();
        
        List<String> columns = new ArrayList<>();
        
        for(Column c : columnArray)
            columns.add(c.getName());

        assertEquals(true, columns.contains("title"));
        assertEquals(true, columns.contains("country"));
        assertEquals(true, columns.contains("city"));
        
        if(columnList.contains(c1))
           columnList.remove(c1);
 
        
        List<GenericPojo> records = jsonParser.getRecords(columnList);
        assertEquals(records.get(0).getValueMap().get(columnList.get(0)).getValue(), "Prosecco Villa Sandi DOC");
        assertEquals(records.get(0).getValueMap().get(columnList.get(1)).getValue(), "Italy");
        assertEquals(records.get(0).getValueMap().get(columnList.get(2)).getValue(), "Napoli");
        
        assertEquals(records.get(1).getValueMap().get(columnList.get(0)).getValue(), "Hugo Deluxe, inclusive");
        assertEquals(records.get(1).getValueMap().get(columnList.get(1)).getValue(), null);
        assertEquals(records.get(1).getValueMap().get(columnList.get(2)).getValue(), "Vienna");

        assertEquals(records.size(), 2);
    }
    
    @Test
    public void testXMLParser(){
        List<Column> columnArray = xmlParser.getColumns();
        
        List<String> columns = new ArrayList<>();
        
        for(Column c : columnArray)
            columns.add(c.getName());
        
        assertEquals(true, columns.contains("title"));
        assertEquals(true, columns.contains("country"));
        assertEquals(true, columns.contains("city"));
        
        if(columnList.contains(c1))
           columnList.remove(c1);
        
        List<GenericPojo> records = xmlParser.getRecords(columnList);
        assertEquals(records.get(0).getValueMap().get(columnList.get(0)).getValue(), "Prosecco Villa Sandi DOC");
        assertEquals(records.get(0).getValueMap().get(columnList.get(1)).getValue(), "Italy");
        assertEquals(records.get(0).getValueMap().get(columnList.get(2)).getValue(), "Napoli");
        
        assertEquals(records.get(1).getValueMap().get(columnList.get(0)).getValue(), "Hugo Deluxe, inclusive");
        assertEquals(records.get(1).getValueMap().get(columnList.get(1)).getValue(), null);
        assertEquals(records.get(1).getValueMap().get(columnList.get(2)).getValue(), "Vienna");

        assertEquals(records.size(), 2);
    }
    
    public String createXmlString() {
        String retVal = null;
        
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder;
            docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("Sekt");
            doc.appendChild(rootElement);

            // aperitif elements
            Element aperitif = doc.createElement("Aperitif");

            // title elements
            Element title = doc.createElement("title");
            title.appendChild(doc.createTextNode("Prosecco Villa Sandi DOC"));
            aperitif.appendChild(title);

            // country elements
            Element country = doc.createElement("country");
            country.appendChild(doc.createTextNode("Italy"));
            aperitif.appendChild(country);

            // city elements
            Element city = doc.createElement("city");
            city.appendChild(doc.createTextNode("Napoli"));
            aperitif.appendChild(city);
            rootElement.appendChild(aperitif);
            
            // aperitif elements
            aperitif = doc.createElement("Aperitif");

            // title elements
            title = doc.createElement("title");
            title.appendChild(doc.createTextNode("Hugo Deluxe, inclusive"));
            aperitif.appendChild(title);

            // city elements
            city = doc.createElement("city");
            city.appendChild(doc.createTextNode("Vienna"));
            aperitif.appendChild(city);
            
            rootElement.appendChild(aperitif);

            // write the content into xml file
            StringWriter sw = new StringWriter();
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            transformer.transform(new DOMSource(doc), new StreamResult(sw));

            retVal = sw.toString();

        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ParserTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(ParserTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(ParserTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return retVal;
    }

   
}
