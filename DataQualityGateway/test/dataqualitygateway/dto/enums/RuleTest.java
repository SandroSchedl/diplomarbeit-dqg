/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataqualitygateway.dto.enums;

import dataqualitygateway.dto.Column;
import dataqualitygateway.dto.CustomizedRule;
import dataqualitygateway.dto.Data;
import dataqualitygateway.dto.GenericPojo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author max
 */
public class RuleTest {

    List<Rule> ruleList;
    Data id, country, usa, twitterAcc, symbol, kryllic, female, date1, date2, credit1, credit2;
    List<GenericPojo> list;

    public RuleTest() {
    }

    @Before
    public void setUp() {
        ruleList = Rule.getRules();
        id = new Data(1);
        country = new Data("Austria");
        usa = new Data("US");
        twitterAcc = new Data("@RyanReynolds");
        symbol = new Data("^^");
        kryllic = new Data("ИЖ");
        female = new Data("Female");
        date1 = new Data("29.01.2000");
        date2 = new Data("29-01-2000");
        credit1 = new Data("4620921533750773");
        credit2 = new Data("5435783453853953905798304");

    }

    /**
     * Test of values method, of class Rule.
     *//*
    @Test
    public void testValueRange() {
        assertEquals(Rule.VALUERANGE.check(new Data(1)), true);
        assertEquals(Rule.VALUERANGE.check(new Data(33000)), false);
        assertEquals(Rule.VALUERANGE.check(new Data("Austria")), false);
        assertEquals(Rule.VALUERANGE.check(new Data(10.1)), true);
        assertEquals(Rule.VALUERANGE.check(new Data(1), new CustomizedRule(0, 2)), true);
        assertEquals(Rule.VALUERANGE.check(new Data(51), new CustomizedRule(0, 2)), false);
        assertEquals(Rule.VALUERANGE.check(new Data("Austria"), new CustomizedRule(0, 2)), false);
        assertEquals(Rule.VALUERANGE.check(new Data(10.1), new CustomizedRule(0, 11)), true);

    }


//    @Test
//    public void testTypeRange() {
//        assertEquals(Rule.TYPECHECKER.check(id), false);
//        assertEquals(Rule.TYPECHECKER.check(country), true);
//    }
  @Test
    public void testIsEurope() {
        assertEquals(Rule.ISEUROPE.check(id), false);
        assertEquals(Rule.ISEUROPE.check(country), true);
        assertEquals(Rule.ISEUROPE.check(usa), false);
    }

    @Test
    public void testIsCountry() {
        assertEquals(Rule.ISCOUNTRY.check(id), false);
        assertEquals(Rule.ISCOUNTRY.check(country), true);
        assertEquals(Rule.ISCOUNTRY.check(usa), false);
    }

    @Test
    public void testAtSign() {
        assertEquals(Rule.ATSIGN.check(id), false);
        assertEquals(Rule.ATSIGN.check(twitterAcc), true);
        assertEquals(Rule.ATSIGN.check(country), false);
    }
//    @Test
//    public void testNotNull() {
//        assertEquals(Rule.NOTNULL.check(id), true);
//        assertEquals(Rule.NOTNULL.check(null), false);
//        assertEquals(Rule.NOTNULL.check(nnull), true);
//        assertEquals(Rule.NOTNULL.check(country), true);
//    }

    @Test
    public void testSymbols() {
        assertEquals(Rule.NOSYMBOLS.check(id), true);
        assertEquals(Rule.NOSYMBOLS.check(symbol), false);
        assertEquals(Rule.NOSYMBOLS.check(country), true);
    }

    @Test
    public void testWritingSystem() {
        assertEquals(Rule.ISWRITINGSYSTEM.check(id), true);
        assertEquals(Rule.ISWRITINGSYSTEM.check(symbol), false);
        assertEquals(Rule.ISWRITINGSYSTEM.check(country), true);
        assertEquals(Rule.ISWRITINGSYSTEM.check(kryllic), false);
    }

    @Test
    public void testExistingCounty() {
        assertEquals(Rule.EXISTINGCOUNTRY.check(id), false);
        assertEquals(Rule.EXISTINGCOUNTRY.check(country), true);
        assertEquals(Rule.EXISTINGCOUNTRY.check(usa), true);
    }

    @Test
    public void testGender() {
        assertEquals(Rule.GENDERFILTER.check(id), false);
        assertEquals(Rule.GENDERFILTER.check(female), true);
        assertEquals(Rule.GENDERFILTER.check(usa), false);
    }

    @Test
    public void testDate() {
        assertEquals(Rule.DATECHECKER.check(id), false);
        assertEquals(Rule.DATECHECKER.check(date2), true);
        assertEquals(Rule.DATECHECKER.check(date1), false);
    }

    @Test
    public void testCaps() {
        assertEquals(Rule.CAPSCHECKER.check(id), true);
        assertEquals(Rule.CAPSCHECKER.check(country), false);
        assertEquals(Rule.CAPSCHECKER.check(usa), true);
    }
     */
    @Test
    public void testCreditCardChecker() {
        assertEquals(Rule.CREDITCARDCHECKER.check(new Data("344306176347975")), true);
        assertEquals(Rule.CREDITCARDCHECKER.check(new Data("1232345353526542")), false);
        assertEquals(Rule.CREDITCARDCHECKER.check(new Data("RandomString")), false);
        assertEquals(Rule.CREDITCARDCHECKER.check(new Data(123)), false);

    }
    /*
    @Test
    public void testTableCheck() {
        list = new ArrayList<>();
        Map<Column, Data> map = new HashMap<>();
        map.put(new Column("PLZ"), new Data("7423"));
        map.put(new Column(""), new Data("7400"));
        GenericPojo gp = new GenericPojo();
        gp.setValueMap(map);
        list.add(gp);
        assertEquals(Rule.TABLECHECKER.check(new Data("7432"), list), false);
        assertEquals(Rule.TABLECHECKER.check(new Data("7423"), list), true);
        assertEquals(Rule.TABLECHECKER.check(new Data("7400"), list), true);
        assertEquals(Rule.TABLECHECKER.check(new Data("1140"), list), false);
    }
     */
}
